# -*- coding: utf-8 -*-
'''
@author: Le Vinh
@note: implement Locally Weighted Regression - Gaussian Kernel function
@version: 1.0
'''

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import math, random, operator

'''@ -------------------- Data Processing -------------------- @ '''
''''
@param fname: file name which containing raw data set, and ratio between training and testing set
@requires: input file in format x1, x2, x3, .., label
@return: trainSet and testSet
'''
def LoadData(fname, ratio):
    fin = open(fname, 'r')
    dataset = []
    for line in fin:
        dataset.append(line.replace('\r\n', '').split(','))
    for i, item in enumerate(dataset):
        if   item[-1] == 'Iris-setosa':     dataset[i][-1] = 1
        elif item[-1] == 'Iris-versicolor': dataset[i][-1] = 2
        elif item[-1] == 'Iris-virginica':  dataset[i][-1] = 3
        dataset[i] = map(float, item)
    random.shuffle(dataset)
    pivot = int(len(dataset)*ratio)
    trainSet = dataset[:pivot]
    testSet  = dataset[pivot:]
    return trainSet, testSet
#LoadData('data/iris.data', 0.67)
'''@ -------------------- Data Processing -------------------- @ '''


'''@ -------------------- Model Construction -------------------- @ '''
''' Compute Euclidean Distance between 2 instances '''
def ComputeEuclidDistance(instance1, instance2, length):
    distance = 0
    for i in range(length):
        distance += pow(instance1[i] - instance2[i], 2)
    return math.sqrt(distance)

''' Get k nearest neighbors with instance from training set '''
def GetNeighbors(trainSet, instance, k):
    distances = []
    length = len(instance) - 1
    for i, item in enumerate(trainSet):
        dist = [ComputeEuclidDistance(item, instance, length)]
        distances.append((trainSet[i], dist))
    distances.sort(key=operator.itemgetter(1))
    kNeighbors = [inst for (inst, dist) in distances[:k]]
    kNeighbors_features = [item[:-1] for item in kNeighbors]
    kNeighbors_features = np.array(kNeighbors_features)
    kNeighbors_labels   = [item[-1] for item in kNeighbors]
    kNeighbors_labels   = np.array(kNeighbors_labels)
    return kNeighbors_features, kNeighbors_labels

''' Visualize k-nearest neighbor '''
def VisualizeFeatures(trainSet, instance, k):
    total_features = np.array([item[:-1] for item in trainSet])
    total_labels   = np.array([item[-1] for item in trainSet])
    neighbors_features, neighbors_labels = GetNeighbors(trainSet, instance, k)
    
    idx1 = 0; idx2 = 2 # feature 0 and feature 2
    total_group    = np.array([feature for feature,_label in zip(total_features,total_labels)])
    neighbor_group = np.array([feature for feature,_label in zip(neighbors_features,neighbors_labels)])
    instance_group = np.array(instance)
    
    plt.plot(total_group.T[idx1], total_group.T[idx2], 'bo', label='training instances')
    plt.plot(neighbor_group.T[idx1], neighbor_group.T[idx2], 'y-^', label='neighbor instances')
    plt.plot(instance_group.T[idx1], instance_group.T[idx2], 'rs', label='query instances')
    plt.legend( loc='upper left', numpoints = 1 ) # note for graph

    plt.title('K Nearest Neighbors')
    plt.xlabel('sepal length [cm]')
    plt.ylabel('petal length [cm]')
    plt.show()

''' Perceptron class '''
class NPPerceptron(object):
    '''
    eta       :  learning rate
    epochs_max:  maximum number of iteration (training cycle)
    '''
    def __init__(self, eta=0.01, epochs=50):
        self.eta = eta
        self.epochs_max = epochs
    
    # Stochastic Gradient Descent in linear formula: y = w0 + w1x1 + w2x2 + w3x3 + w4x4
    def TrainStochastic(self, X, y, instance):
        finstance = np.array(instance[:-1]) # feature of query instance
        length    = len(instance) - 1
        self.name   = 'Stochastic Gradient Descent'
        self.weight = np.zeros(1 + X.shape[1])
        self.errors = []
        self.epochs_count = 0
        for _ in range(self.epochs_max):
            self.epochs_count += 1
            for xi, target in zip(X, y):
                output = self.Predict(xi)
                error = ComputeEuclidDistance(xi,finstance,length) * (target - output) # Weighted regression
                #error = (target - output) # Non-weighted regression
                self.weight[1:] += self.eta * (xi * error) # weight vector = vector xi * value error
                self.weight[0]  += self.eta * error
            cost = ((y - self.Predict(X))**2).sum() / 2.0
            self.errors.append(cost)
        return self
    
    def Predict(self, X):
        return 1*self.weight[0] + np.dot(X, self.weight[1:])
    
    def PrintModel(self):
        print 'After training %d times' % self.epochs_count 
        print 'Weight vector: ', map(str,self.weight)
        scr = 'Target function: ' + str(self.weight[0]) + ' + '
        for idx,wi in enumerate(self.weight[1:]): scr += str(wi) + '*x' + str(idx+1) + ' + '
        print scr[:-2]
    
    def EvaluateLocalModel(self, features, labels):
        count = 0
        for X, target in zip(features,labels): # loop over all data examples
            print target, self.NetInput(X)
            if target == round(self.Predict(X)): count += 1
        print "The accuracy of model: %.2f %%" % (100*count/len(features))
        plt.plot(range(1, len(self.errors)+1), self.errors, marker='o')
        plt.title('Training Error in '+self.name)
        plt.xlabel('Iterations')
        plt.ylabel('Error classification')
        plt.axis([1, len(self.errors)+1, 0, max(self.errors)+1])
        plt.show()
'''@ -------------------- Model Construction -------------------- @ '''

'''@ -------------------- Model Evaluation -------------------- @ '''
def main():
    k = 10
    trainSet, testSet = LoadData('data/iris.data', 0.67)
    ppn = NPPerceptron(eta=0.0001, epochs=100)
    
    # Predict a query instance
    kNeighbors_features, kNeighbors_labels = GetNeighbors(trainSet, testSet[1], k)
    VisualizeFeatures(trainSet, testSet[1], k)
    ppn.TrainStochastic(kNeighbors_features, kNeighbors_labels, testSet[1])
    print 'Target output: %f and Predicted output: %f' % (testSet[1][-1], ppn.Predict(testSet[1][:-1]))
    
    # Evaluate model
    correct = 0
    for item in testSet:
        output = round(ppn.Predict(item[:-1]))
        #print item[-1], output
        if item[-1] == output: correct += 1
    print 'The correct percentage of: %s %%' % str((correct/len(testSet)) * 100.0)
main()