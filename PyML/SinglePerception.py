# -*- coding: utf-8 -*-
'''
@author: Le Vinh
@note: implement Single Perception Learning in Python
       idea programming from http://sebastianraschka.com/Articles/2015_singlelayer_neurons.html
       data source: https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data
       full data source: http://goo.gl/j0Rvxq
@version: 1.0
'''

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

'''@ -------------------- Data Processing -------------------- @ '''
''''
@param fname: file name which containing raw data set
@requires: input file in format x1, x2, x3, .., label
@return: list of input features and list of label over training set
'''
def LoadData(fname):
    fin = open(fname, 'r')
    dataset = []
    for line in fin:
        dataset.append(line.replace('\r\n', '').split(','))
    features = [map(float, inst[:-1]) for inst in dataset] # List of feature input
    labels   = [inst[-1] for inst in dataset]              # List of label
    tmp      = labels[0]
    for idx,label in enumerate(labels):                    # Normalization label: -1 , 1
        labels[idx] = -1 if (label == tmp) else 1
    return features, labels
#LoadData('data/iris.data')
def NPLoadData(fname):
    fin      = open(fname, 'r')
    dataset  = [line.replace('\r\n', '').split(',') for line in fin]
    features = [map(float, inst[:-1]) for inst in dataset] # List of feature input
    labels   = [inst[-1] for inst in dataset]              # List of label
    tmp      = labels[0]
    for idx,_label in enumerate(labels):                    # Normalization label: -1 , 1
        labels[idx] = -1 if (_label == tmp) else 1
    features = np.array(features)
    labels   = np.array(labels)
    return features, labels
'''@ -------------------- Data Processing -------------------- @ '''


'''@ -------------------- Model Construction -------------------- @ '''
class Perceptron(object):
    '''
    eta       :  learning rate
    epochs_max:  maximum number of iteration (training cycle)
    '''
    def __init__(self, eta=0.01, epochs=100):
        self.eta = eta
        self.epochs_max = epochs
    
    '''
    features : list of vector input
    labels   : list of label output
    weight   : list of vector weight
    errors   : error after each iteration 
    '''
    def TrainPerceptionRule(self, features, labels):
        self.weight  = [0] * (len(features[1])+1)  # list of zeros in size (number of features + 1)
        self.errors_ = []
        self.epochs_count = 0
        
        for _ in range(self.epochs_max):
            self.epochs_count += 1
            flag = 0        # whether model is true for all examples
            for X, target in zip(features,labels): # loop over all data examples
                X = [1] + X # add x0 = 1 for w0
                error_rate = self.eta * (target - self.Predict(X))
                if (flag == 0) & (target != self.Predict(X)): flag = 1
                for idx,_ in enumerate(self.weight):     # update weight
                    self.weight[idx] += error_rate * X[idx]
            if (flag == 0): return self
        self.PrintModel()
        return self
    
    # Batch Gradient Descent
    def TrainDeltaRule(self, features, labels):
        self.weight  = [0] * (len(features[1])+1)
        self.epochs_count = 0
        for _ in range(self.epochs_max):
            self.epochs_count += 1
            error_rate = [0] * (len(features[1])+1)
            for X, target in zip(features,labels): # loop over all data examples
                X    = [1] + X # add x0 = 1 for w0
                diff = target - self.Predict_Unthreshold(X)
                #print self.Predict_Unthreshold(X)
                for idx in range(len(error_rate)):
                    error_rate[idx] += diff * X[idx]
            #print self.weight
            for idx in range(len(self.weight)): # loop over the number of weight
                self.weight[idx] += self.eta * error_rate[idx]
        return self
    
    # X: a single input (list of features)
    def NetInput(self, X):
        func = 0.0
        for x, w in zip(X,self.weight): # w0 + w1.x1 + ... + wn.xn (dot product of two arrays)
            func += x*w
        return func
    def Predict(self, x):
        if self.NetInput(x) >= 0.0: return 1
        else: return -1
    def Predict_Unthreshold(self, x):
        return self.NetInput(x)
    
    def PrintModel(self):
        print 'After training %d times' % self.epochs_count 
        print 'Weight vector: ', map(str,self.weight)
        scr = 'Target function: ' + str(self.weight[0]) + ' + '
        for idx,wi in enumerate(self.weight[1:]): scr += str(wi) + '*x' + str(idx+1) + ' + '
        print scr[:-2]
    
    def EvaluateModel(self, features, labels):
        count = 0
        for X, target in zip(features,labels): # loop over all data examples
            X = [1] + X # add x0 = 1 for w0
            if target == self.Predict(X): count += 1
        print "The accuracy of model: %.2f %%" % (100*count/len(features))


# Implementation perception by supporting in calculation from Numpy
class NPPerceptron(object):
    '''
    eta       :  learning rate
    epochs_max:  maximum number of iteration (training cycle)
    '''
    def __init__(self, eta=0.01, epochs=50):
        self.eta = eta
        self.epochs_max = epochs
    
    '''
    X : list of vector input
    y : list of label output
    weight : list of vector weight
    errors : error after each iteration 
    '''
    def TrainPerceptionRule(self, X, y):
        self.name    = 'Perception Rule'
        self.weight  = np.zeros(1 + X.shape[1]) # X.shape = [number of input instance, number of features]
        self.errors  = []
        self.epochs_count = 0
        for _ in range(self.epochs_max):
            self.epochs_count += 1
            flag    = 0        # whether model is true for all examples
            errors  = 0        # error in each epochs
            for xi, target in zip(X,y):
                error_rate = self.eta * (target - self.Predict_Threshold(xi))
                if (error_rate != 0): flag = 1
                self.weight[1:] += error_rate * xi
                self.weight[0]  += error_rate
                errors          += int(error_rate != 0.0)
            self.errors.append(errors)
            if (flag == 0): return self
        return self
    
    # Batch Gradient Descent
    def TrainDeltaRule(self, X, y):
        self.name   = 'Batch Gradient Descent'
        self.weight = np.zeros(1 + X.shape[1])
        self.errors = []
        self.epochs_count = 0
        for _ in range(self.epochs_max):
            self.epochs_count += 1
            output = self.Predict_Unthreshold(X)
            errors = y - output   # here errors is a vector = vector y - vector output
            self.weight[1:] += self.eta * X.T.dot(errors) # matrix transpose of X * vector errors (* is vector product)
            self.weight[0]  += self.eta * errors.sum()
            cost_funct       = (errors**2).sum() / 2.0
            self.errors.append(cost_funct)
        return self
    
    # Stochastic Gradient Descent
    def TrainStochastic(self, X, y):
        self.name   = 'Stochastic Gradient Descent'
        self.weight = np.zeros(1 + X.shape[1])
        self.errors = []
        self.epochs_count = 0
        for _ in range(self.epochs_max):
            self.epochs_count += 1
            for xi, target in zip(X, y):
                output = self.Predict_Unthreshold(xi)
                error = (target - output) # here errors is a value = target - output
                #self.weight[1:] += self.eta * xi.dot(error) 
                self.weight[1:] += self.eta * (xi * error) # weight vector = vector xi * value error
                self.weight[0]  += self.eta * error
            cost = ((y - self.Predict_Unthreshold(X))**2).sum() / 2.0
            self.errors.append(cost)
        return self
    
    def NetInput(self, X):
        return 1*self.weight[0] + np.dot(X, self.weight[1:])
    
    def Predict_Threshold(self, X):
        return np.where(self.NetInput(X) >= 0.0, 1, -1)
    def Predict_Unthreshold(self, X):
        return self.NetInput(X)
    
    def PrintModel(self):
        print 'After training %d times' % self.epochs_count 
        print 'Weight vector: ', map(str,self.weight)
        scr = 'Target function: ' + str(self.weight[0]) + ' + '
        for idx,wi in enumerate(self.weight[1:]): scr += str(wi) + '*x' + str(idx+1) + ' + '
        print scr[:-2]
    
    def EvaluateModel(self, features, labels):
        count = 0
        for X, target in zip(features,labels): # loop over all data examples
            if target == self.Predict_Threshold(X): count += 1
        print "The accuracy of model: %.2f %%" % (100*count/len(features))
        plt.plot(range(1, len(self.errors)+1), self.errors, marker='o')
        plt.title('Training Error in '+self.name)
        plt.xlabel('Iterations')
        plt.ylabel('Error classification')
        plt.axis([1, len(self.errors)+1, 0, max(self.errors)+1])
        plt.show()
    
    def VisualizeFeatures(self, features, labels):
        idx1 = 0
        idx2 = 2
        _group = np.array([feature for feature,label in zip(features,labels) if label == -1])
        group  = np.array([feature for feature,label in zip(features,labels) if label == 1])        
        #plt.plot(group.T[idx1], group.T[idx2], 'ro')
        plt.plot(group.T[idx1], group.T[idx2], 'ro', _group.T[idx1], _group.T[idx2], 'b^')
        plt.title('Perceptron Classification')
        plt.xlabel('sepal length [cm]')
        plt.ylabel('petal length [cm]')
        plt.show()
    
'''@ -------------------- Model Construction -------------------- @ '''

'''@ -------------------- Model Evaluation -------------------- @ '''
# Test program
def main():
    print '***** Implement perception by pure Python *****'
    features, labels = LoadData('data/iris.data')
    #features, labels = LoadData('data/or.data')
    ppn1 = Perceptron(eta=0.0001, epochs=50) # if choose learning rate bigger, such as 0.01 -> fail in delta rule
    print '- Perception rule:'
    ppn1.TrainPerceptionRule(features, labels)
    ppn1.PrintModel()
    ppn1.EvaluateModel(features, labels)
    print '- Delta rule (Batch Gradient Descent):'
    ppn1.TrainDeltaRule(features, labels)
    ppn1.PrintModel()
    ppn1.EvaluateModel(features, labels)
    
    print '\n\n***** Implement perception with Numpy -> Recommendation in real work *****'
    features, labels = NPLoadData('data/iris.data')    
    # if choose learning rate bigger, e.g. 0.01 -> fail (very low accuracy & over flow computation)
    ppn2 = NPPerceptron(eta=0.0001, epochs=20)
    ppn2.VisulizeFeatures(features,labels)
    print '- Perception rule:'
    ppn2.TrainPerceptionRule(features, labels)
    ppn2.PrintModel()
    ppn2.EvaluateModel(features, labels)
    print '- Delta rule (Batch Gradient Descent):'
    ppn2.TrainDeltaRule(features, labels)
    ppn2.PrintModel()
    ppn2.EvaluateModel(features, labels)
    print '- Delta rule (Stochastic Gradient Descent):'
    ppn2.TrainStochastic(features, labels)
    ppn2.PrintModel()
    ppn2.EvaluateModel(features, labels)

main()