# -*- coding: utf-8 -*-
'''
@author: Le Vinh
@note: implement Naive Bayes From Scratch in Python
       idea programming from http://machinelearningmastery.com/naive-bayes-classifier-scratch-python/
       For discrete value
@version: 1.0
@weak_point: Pure Naive Bayes can not deal with unseen feature value in a good way
@strong_point: Pure Naive Bayes can be applied to any data type
'''

from __future__ import division
import random, pandas

'''@ -------------------- Data Processing -------------------- @ '''
''''
@param fname: file name which containing raw data set
@requires: input file in format x, y, z, t
@return: nested list
'''
def LoadData(fname):
    fin = open(fname, 'r')
    dataset = []
    for line in fin:
        if ((len(line)>1) & (not line.startswith('@')) & (not line.startswith('%'))):
            dataset.append(line.replace('\n', '').split(','))
    return dataset

''''
@note: split data set into train set and test set by a certain ratio
@param dataset: nested list from LoadData function
@param ratio: division ratio 
@return: pair of train set and test set
'''
def SplitDataByRatio(dataset, ratio):
    trainSize = int(len(dataset) * ratio)
    trainSet  = []
    testSet   = list(dataset)
    while len(trainSet) < trainSize:
        idx = random.randrange(len(testSet))
        trainSet.append(testSet.pop(idx))
    return [trainSet, testSet]
#SplitDataByRatio(LoadData('/data/weather.arff'), 0.8)

''''
@note: split dataset into sets by classes
@param dataset: nested list from SplitDataByRatio function
@return: dictionary {hypothesis: [[instance1_features],[instance2_features]]} 
'''
def SplitDataByClass(dataset):
    classes = {}
    for item in dataset:
        if item[-1] not in classes:
            classes[item[-1]] = []
        classes[item[-1]].append(item[:-1])
    return classes
#SplitDataByClass(LoadData('data/weather.arff'))
'''@ -------------------- Data Processing -------------------- @ '''


'''@ -------------------- Model Construction -------------------- @ '''
''' Print model in table style '''
# http://stackoverflow.com/questions/18746278/building-a-table-from-python-nested-dictionaries-with-missing-values
def PrintModel(model):
    table = pandas.DataFrame(model)
    table.fillna(0, inplace=True)
    print(table)

''''
@note: construct Naive Bayes Model from train set (Model here is set of P(feature|hypothesis)
@param trainSet: from SplitDataByClass
@return: dictionary { hypothesis: {{feature1: value1},{feature2: value2}} }
        value1 = P(feature1|hypothesis), value2 = P(feature2|hypothesis)
'''
def TrainNaiveBayesModel(trainSet):
    trainSize = sum([len(tmp) for tmp in trainSet.values()])
    
    # Count frequency distribution
    FreqDist = {} # dict with key is class, and sub-dict with key is feature/attribute
    for clas in trainSet:
        if clas not in FreqDist: FreqDist[clas] = {}
        for record in trainSet[clas]:
            for feature in record:
                if feature not in FreqDist[clas]:
                    FreqDist[clas][feature] = 1
                else: FreqDist[clas][feature] += 1        # Freq(feature|hypothesis)
        FreqDist[clas]['*TOTAL'] = len(trainSet[clas])    # Freq(hypothesis)
    #print '- Table of frequency of class-attribute \n', PrintModel(FreqDist)
    
    # Compute probability distribution = Naive Bayes model
    model = {}
    for clas in FreqDist:
        if clas not in model: model[clas] = {}
        for feature in FreqDist[clas]:
            if feature not in model[clas]: model[clas][feature] = 0
            if feature == '*TOTAL':
                model[clas][feature] = FreqDist[clas][feature] / trainSize                # Prior probability P(hypothesis)
            else:
                model[clas][feature] = FreqDist[clas][feature] / FreqDist[clas]['*TOTAL'] # Likelihood probability P(feature|hypothesis)
    #print '- Table of probabilities of class-attribute \n', PrintModel(model)
    return  model
#TrainNaiveBayesModel(SplitDataByClass(LoadData('data/weather.arff')))
'''@ -------------------- Model Construction -------------------- @ '''


'''@ -------------------- Model Evaluation -------------------- @ '''
'''
@note: predict class a new instance by Naive Bayes formula (traditional for discrete value)
@param model: Bayes model from TrainNaiveBayesModel
@param instance: list values of new instance
@attention: there is a problem here with a new feature (not appear in training) then P(new_featr|h) = 0
'''
def PredictInstanceNaiveBayes(model, instance):
    # Compute probability of hypothesis h = P(h).P(featr1|h).P(featr2|h)...P(featr_n|h)
    hypothesis = {}
    for clas in model:
        target_funct = model[clas]['*TOTAL']
        for attr in instance:
            if attr in model[clas]:
                target_funct *= model[clas][attr]
            else: target_funct *= 0
        hypothesis[clas] = target_funct
    #for clas in hypothesis: print 'Probability of \'%s\': %f' % (clas, hypothesis[clas])
    
    # Result normalization
    normal = {}
    for clas in hypothesis:
        normal[clas] = hypothesis[clas] / sum([val for val in hypothesis.values()])
    #for clas in normal: print 'Probability of \'%s\' after normalizing: %.3f ' % (clas, normal[clas])
    
    # Decision making
    print '----- Decesion making -----'
    max_target_value = (max([val for val in normal.values()]))
    max_hypothesis = [clas for clas, val in normal.items() if val == max_target_value][0]
    prediction = 'Instance with '
    for feature in instance:
        prediction += str(feature) + ' + '
    prediction = prediction[:-2] + ' -> ' + str(max_hypothesis) + ' in ' + "%.3f" % max_target_value + ' %'
    print prediction
    return max_hypothesis
    
'''
@note: predict class a new instance by Naive Bayes formula
@param model: Bayes model from TrainNaiveBayesModel
@param instance: list values of new instance 
'''
def EvaluateModel(model, testset):
    testSize    = sum([len(tmp) for tmp in testset.values()])
    num_correct = 0
    for clas in testset:
        for item in testset[clas]:
            if clas == PredictInstanceNaiveBayes(model, item):
                num_correct += 1
    print "Evaluation: %.3f %% correct" % (100*num_correct / testSize)
    return (num_correct / testSize)
'''@ -------------------- Model Evaluation -------------------- @ '''
    
# Test program
def main():
    dataset = LoadData('data/weather.nominal.arff')
    trainset, testset = SplitDataByRatio(dataset, 0.9)
    trainset = SplitDataByClass(trainset)
    testset  = SplitDataByClass(testset)
    model    = TrainNaiveBayesModel(trainset)    
    
    instance = ['sunny', 'cool', 'high', 'TRUE']
    PredictInstanceNaiveBayes(model, instance)
    EvaluateModel(model, testset)
#main()