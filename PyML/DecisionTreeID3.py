# -*- coding: utf-8 -*-
'''
@author: Le Vinh
@note: implement Decision Tree ID3 algorithm
        idea programming from http://jeremykun.com/tag/decision-trees/
@version: 1.0
'''

from __future__ import division
import math

'''@ -------------------- Data Processing -------------------- @ '''
class TreeNode:
    def __init__(self, parent=None):
        self.parent             = parent 
        self.children           = []
        self.deep               = 0     # deep level of node
        self.splitFeature       = None  # the feature for which each of its children assumes a separate value
        self.splitFeatureValue  = None  # the feature assumed for its parent’s split
        self.label              = None  # the final classification label for a leaf

''''
@param fname: file name which containing raw data set
@requires: input file in format x, y, z, t
@return: nested list of (features, label)
'''
def LoadData(fname):
    fin = open(fname, 'r')
    dataset = []
    for line in fin:
        if ((len(line)>1) & (not line.startswith('@')) & (not line.startswith('%'))):
            instance = line.replace('\n', '').split(',')
            features = instance[:-1]
            label    = instance[-1]
            dataset.append((features, label))
    return dataset
#LoadData('data/weather.nominal.arff')

'''
@param data: nested list data from LoadData
@param featureIndex: an index of feature in the list of feature 
@return: list of subset data by all possible values of an given feature
'''
def SplitData(data, featureIndex):
    attr_vals = [feature[featureIndex] for (feature, label) in data] # possible values of the given feature
    dataSubset = []
    for val in set(attr_vals):
        tmp = [(feature, label) for (feature, label) in data if feature[featureIndex] == val]
        dataSubset.append(tmp)
    #for tmp in dataSubset: print tmp
    return dataSubset
#SplitData(LoadData('data/weather.nominal.arff'), 3)

'''
@param data: nested list data from LoadData
@return: entropy of data set
'''
def GetEntropy(data):
    labels    = [label for (_features, label) in data]
    trainSize = len(labels)
    freqDist  = {}
    for label in labels:
        if label not in freqDist: freqDist[label] = 0
        freqDist[label] += 1
    probDist  = {}
    for label in freqDist:
        probDist[label] = freqDist[label] / trainSize
    entropy = -sum([probDist[lb] * math.log(probDist[lb],2) for lb in probDist])
    #print 'dataset entropy = %.3f' % entropy
    return entropy
#GetEntropy(LoadData('data/weather.nominal.arff'))

'''
@param data: nested list data from LoadData
@param featureIndex: an index of feature in the list of feature 
@return: information gain value
'''
def GetInfoGain(data, featureIndex):
    infoGain = GetEntropy(data)
    dataSubset = SplitData(data, featureIndex)
    for subset in dataSubset:
        infoGain -= GetEntropy(subset) * (len(subset)/len(data))
    #print 'Information Gain value of wind feature: %.3f' % infoGain
    return infoGain
#GetInfoGain(LoadData('data/weather.nominal.arff'), 3)
'''@ -------------------- Data Processing -------------------- @ '''


'''@ -------------------- Model Construction -------------------- @ '''

def PrintTreeModel(node, features_name):
    #<Split feature> | Constraint formula | <Label>
    splitFeature        = None
    splitFeatureValue   = None
    label               = ''
    if node.splitFeature is not None:
        splitFeature = features_name[int(node.splitFeature)]
    if node.splitFeatureValue is not None:
        splitFeatureValue = '(%s = %s)' % (features_name[int(node.parent.splitFeature)], node.splitFeatureValue)
    if node.label is not None:
        label = node.label
    print '  ' * node.deep,
    print '<%s> | %s | %s' % (splitFeature, splitFeatureValue, label)
    
    if len(node.children) != 0:
        for child in node.children:
            PrintTreeModel(child, features_name)
            
''' 
@param data: nested list data
@return: check our input data all have the same classification label? true : false
Return True if the data have the same label, and False otherwise. '''
def Homogeneous(data):    
    feature_num = len(set([label for (_features, label) in data]))
    return feature_num <= 1

''' Label node with the majority of the class labels in the given data set. '''
def MajorityVote(data, node):
    labels = [label for (_features, label) in data]
    node.label = max(set(labels), key=labels.count)
    return node

'''
@param data: nested list data
@param root: root node (which may be the root of a subtree)
@return: decision tree from the given data
'''
def TrainDecisionTree(data, root, remainFeatures):
    # Base case 1: all have the same classification label
    if Homogeneous(data):
        root.label = data[0][1]
        return root
    # Base case 2: run out of features
    if len(remainFeatures) == 0:
        return MajorityVote(data, root)
    
    # Find the best feature (index) to split data
    bestFeature = max(remainFeatures, key=lambda index: GetInfoGain(data, index))
    root.splitFeature = bestFeature

    # Add child nodes and process recursively
    for dataSubset in SplitData(data, bestFeature):
        child = TreeNode(parent=root)
        child.splitFeatureValue = dataSubset[0][0][bestFeature]
        child.deep = root.deep + 1
        root.children.append(child)
        remainFeatures -= set([bestFeature])
        TrainDecisionTree(dataSubset, child, remainFeatures)
    return root
'''@ -------------------- Model Construction -------------------- @ '''


'''@ -------------------- Model Evaluation -------------------- @ '''
'''
@note: predict class a new instance by decision tree
@param tree: decision tree model
@param instance: list values of new instance
@param features_name: list feature names
'''
def PredictInstance(tree, instance, features_name):
    ''' Classify a data point by traversing the given decision tree. '''
    if tree.children == []:
        return tree.label
    else:
        for child in tree.children:
            if child.splitFeatureValue == instance[tree.splitFeature]:
                return PredictInstance(child, instance, features_name)
def PredictInstancePrint(tree, instance, features_name):
    ''' Classify a data point by traversing the given decision tree. '''
    if tree.children == []:
        return tree.label
    else:
        for child in tree.children:
            if child.splitFeatureValue == instance[tree.splitFeature]:
                print '('+features_name[tree.splitFeature] + ' = ' + child.splitFeatureValue+') ->',
                return PredictInstance(child, instance, features_name)

'''
@note: predict class a new instance by decision tree model
@param data: test data set
@param tree: decision tree model
'''
def EvaluateModel(data, tree, features_name):
    testSize    = len(data)
    num_correct = 0
    for item in data:
        if item[1] == PredictInstance(tree, item[0], features_name):
            num_correct += 1
    print "Evaluation: %.3f %% correct" % (100*num_correct / testSize)
    return (num_correct / testSize)
'''@ -------------------- Model Evaluation -------------------- @ '''

# Test program
def main():
    data     = LoadData('data/weather.nominal.arff')
    features_name = ['outlook', 'temperature', 'humidity', 'windy']
    features = set(range(len(data[0][0]))) # set of index features from 0 -> number of features 
    tree     = TrainDecisionTree(data, TreeNode(), features)
    print 'Decision Tree Model'
    PrintTreeModel(tree, features_name)
    
    instance = ['sunny', 'cool', 'high', 'TRUE']
    print '\nInstance(',
    for feature in instance: print feature,
    print ') \n --> predict: ', PredictInstancePrint(tree, instance, features_name)
    EvaluateModel(data, tree, features_name)
main()