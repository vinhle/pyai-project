# This program is an machine learning experiment with the FindS concept learning algorithm
# Based on an excercise from Machine Learning by Thomas Mitchell (1997)
#
# The attribute EnjoySport indicates whether or not Aldo enjoys his favorite water sport on this day
#
# For all possible days with the following attributes:
# Sky: Sunny/Rainy
# AirTemp: Warm/Cold
# Humidity: Normal/High
# Wind: Strong/Weak
# Water: Warm/Cool
# Forecast: Same/Change
#
# Let us represent the hypothesis with the vector:
# [Sky, AirTemp, Humidity, Wind, Water, Forecast]
#
# Where each constraint may be '?' to represent that any value is acceptable,
# '0' to represent that no value is acceptable, or a specific value (from above)
#
# A training example for the hypothesis is True if it correctly predicts that
# Aldo will enjoy his water sport on this day, and False otherwise

attributes = [['Sunny','Rainy'],
              ['Warm','Cold'],
              ['Normal','High'],
              ['Strong','Weak'],
              ['Warm','Cool'],
              ['Same','Change']]

# Check more general partial order between attributes
# attr2 >= attr1 if (attr2 == attr1) or (attr2 > attr1, in this case attr2 == '?')
def CheckMoreGenearlPartialOrder(attr1, attr2):
    if attr2 == '?': return True
    elif attr1 == attr2: return True
    else: return False

# Find S algorithms
def FindS(dataSet):
    positiveSet = [item for item in dataSet if item[-1] == 'Yes']
    hypothesis  = positiveSet[0][:-1]
    for instance in positiveSet:
        for i in range(len(instance[:-1])):
            if CheckMoreGenearlPartialOrder(instance[i], hypothesis[i]) == False:
                hypothesis[i] = '?'
        print 'Hypothesis:', hypothesis
    return hypothesis

# Evaluate model
def Evaluate(dataSet, hypothesis):
    output = ['Yes'] * len(dataSet)
    count  = 0
    for i in range(len(dataSet)):
        for j in range(len(dataSet[i][:-1])):
            if CheckMoreGenearlPartialOrder(dataSet[i][j], hypothesis[j]) == False:
                #print dataSet[i][j], hypothesis[j]
                output[i] = 'No'
    for i in range(len(dataSet)):
        if output[i] == dataSet[i][-1]: count += 1
    print 'Output:', output
    print 'Evaluation (correct percentage): %.2f %%' % 100*(count/len(dataSet))
    return output
    

def main():
    dataSet = [['Sunny', 'Warm', 'Normal', 'Strong', 'Warm', 'Same',   'Yes'],
                ['Sunny', 'Warm', 'High',   'Strong', 'Warm', 'Same',   'Yes'],
                ['Rainy', 'Cold', 'High',   'Strong', 'Warm', 'Change', 'No'],
                ['Sunny', 'Warm', 'Normal', 'Strong', 'Cool', 'Change', 'Yes'],]
    hypothesis = FindS(dataSet)
    Evaluate(dataSet, hypothesis)
main()
    