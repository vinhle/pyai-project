# -*- coding: utf-8 -*-
'''
@author: Le Vinh
@note: implement Logistic Regression
        @weak: don't find the reason cause the not good result
@version: 1.0
'''

from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

def NPLoadData(fname):
    fin      = open(fname, 'r')
    dataset  = [line.replace('\n', '').split(',') for line in fin]
    features = [map(float, inst[:-1]) for inst in dataset[1:]] # List of feature input
    labels   = [float(inst[-1]) for inst in dataset[1:]]              # List of label
    features = np.array(features)
    labels   = np.array(labels)
    return features, labels

# Stochastic Gradient Descent
def TrainStochastic(X, y, eta=0.000005, epochs_max=1000):
    weight = np.zeros(1 + X.shape[1])
    #weight = np.random.rand(1 + X.shape[1])
    errors = []
    epochs_count = 0
    for _ in range(epochs_max):
        epochs_count += 1
        for xi, target in zip(X, y):
            output = Sigmoid(Predict(xi, weight))       
            weight[1:] += eta * output * (1-output) * (target - output) * xi
            weight[0]  += eta * output * (1-output) * (target - output)
        cost = ((y - Predict(X,weight))**2).sum() / 2.0
        print weight
        errors.append(cost)
    return weight, errors

def Predict(X, weight):
    return 1*weight[0] + np.dot(X, weight[1:])
def Sigmoid(y):
    return 1.0/(1.0 + np.exp(-y))

def PrintModel(weight):
    print 'Weight vector: ', map(str,weight)
    scr = 'Target function: y = ' + str(weight[0]) + ' + '
    for idx,wi in enumerate(weight[1:]): scr += str(wi) + '*x' + str(idx+1) + ' + '
    print scr[:-2]

def EvaluateModel(features, labels, weight, errors):
    count = 0
    means = sum(labels)/len(labels)
    threshold = round(means*5/100)
    for X, target in zip(features,labels): # loop over all data examples
        #print target, Predict(X,weight)
        if abs(target - Predict(X,weight)) <= threshold: count += 1
    print "The accuracy of model: %.2f %%" % (100*count/len(features))
    plt.plot(range(1, len(errors)+1), errors, marker='o')
    plt.title('Training Error in Stochastic Gradient Descent')
    plt.xlabel('Iterations')
    plt.ylabel('Error classification')
    plt.axis([1, len(errors)+1, 0, max(errors)+1])
    plt.show()

def LinearFunct(X, weight):
    y = weight[0]
    for i in range(len(weight[1:])):
        y += X[i] * weight[i]
    return y
def VisualizeFeatures(features, labels, weight):
    predict = []
    for X in features:
        predict += [Predict(X, weight)]
    plt.plot(features, labels, 'bo')
    plt.plot(features, predict, 'k')
    plt.title('Linear Regression Classification')
    plt.xlabel('Height')
    plt.ylabel('Weight')
    plt.show()

def main():
    print '***** Implement linear regression by pure Python *****'
    features, labels = NPLoadData('data/weight.data')
    weight,   errors = TrainStochastic(features, labels)
    print errors
    PrintModel(weight)
    EvaluateModel(features, labels, weight, errors)
    VisualizeFeatures(features, labels, weight)
    
main()