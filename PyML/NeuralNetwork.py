# -*- coding: utf-8 -*-
'''
@author: Le Vinh
@note: implement Neural Network Learning Model from scratch in Python
        - http://www.bogotobogo.com/python/python_Neural_Networks_Backpropagation_for_XOR_using_one_hidden_layer.php
        original source code is wrong in error term formula (double sigmoid function)
        - http://www.wildml.com/2015/09/implementing-a-neural-network-from-scratch/
@version: 1.0
'''

import numpy as np
import matplotlib.pyplot as plt

'''
@note: formula for error term and  weight update are different in Sigmoid and Tanh function,
because the difference occurs in derivation (see ex4.9 page 125 - book Machine Learning, Tom M.Mitchell
- sigmoid'(y) = sigmoid(y)(1 - sigmoid(y))
- tanh(y)     = 1 - tanh(x)**2
'''
def Sigmoid(y):
    return 1.0/(1.0 + np.exp(-y))
def Tanh(y):
    return np.tanh(y)


class NeuralNetwork:
    '''
    @layers: structure of network (2 layers). E.g. [2,2,1]: 2 input units, 2 hidden units, 1 output unit
    '''
    def __init__(self, layers, activation='sigmoid'):
        if activation == 'sigmoid': self.activation = Sigmoid
        elif activation == 'tanh':  self.activation = Tanh
               
        # weight initialization procedure
        self.weights = [] # each element is a 2-dimensional weight matrix from layer i to layer j
        # layers = [2,2,1]
        # range of weight values (-1,1)
        # input and hidden layers - random((2+1, 2+1)) -> weight matrix 3 x 3
            # np.random.random((i,j)) -> random a matrix in size [i x j]
            # size of weight matrix from layer i -> j (including extra unit x0 and except output layer):
            # [((number of unit in layer i) + 1) x ((number of unit in layer j) + 1)]
        for i in range(1, len(layers)-1):
            r = 2*np.random.random((layers[i-1]+1, layers[i]+1)) - 1
            self.weights.append(r)
        # output layer - random((2+1, 1)) -> weight matrix 3 x 1
        r = 2*np.random.random( (layers[i] + 1, layers[i+1])) - 1
        self.weights.append(r)
    
    ''' Stochastic gradient descent
    X : list of vector input
    y : list of output value
    '''
    def SigmoidBackpropagationTraining(self, X, y, learning_rate=0.1, epochs=10000):
        # Add column of ones to X
        # This is to add the bias unit to the input layer
        ones = np.atleast_2d(np.ones(X.shape[0]))
        X    = np.concatenate((ones.T, X), axis=1)
        self.error_rate = []
         
        for k in range(epochs):
            #if k % 100 == 0: print 'epochs:', k
            for i in range(len(X)): # For each training example
                # -- Step 1: Input instance and compute output of every units in the network
                inputs = [X[i]]      # initialize input vectors as [original input vector]   
                for l in range(len(self.weights)):  # l means the position of layer
                    # dot product input vector and weight matrix vector at layer l -> dot_value vector at layer l+1
                    dot_value  = np.dot(inputs[l], self.weights[l])
                    # output vector of units at layer l+1
                    output     = Sigmoid(dot_value)
                    # output of units at layer l+1 as input of units at layer l+2
                    inputs.append(output)
                # -- Step 1 end.
                
                # -- Step 2 + 3: Calculate error terms in hidden and output units
                # Output layer
                output = inputs[-1]
                errors = [output * (1-output) * (y[i] - output)] # --- sigmoid function ---
                #errors = [Sigmoid(output) * (1-Sigmoid(output)) * (y[i] - output)] # equivalent to original code
                # Squared error rate: for visualization
                _err   = []
                for tmp in y[i] - output: _err += [tmp * tmp]
                if i == 1: self.error_rate.append(sum(_err) / 2.0)
    
                # Hidden layers: we need to begin at the second to last layer 
                # (a layer before the output layer)
                for l in range(len(inputs) - 2, 0, -1):
                    output = inputs[l]
                    errors.append(output * (1-output) * errors[-1].dot(self.weights[l].T))  # --- sigmoid function ---
                    #errors.append(Sigmoid(output) * (1-Sigmoid(output)) * errors[-1].dot(self.weights[l].T)) # equivalent to original code
    
                # Reverse the order of errors term vectors
                # e.g. [level3(output)->level2(hidden)]  => [level2(hidden)->level3(output)]
                errors.reverse()
                # -- Step 2 + 3 end.
                
                # -- Step 4: Update network weights -> backpropagation
                # 1. Multiply its output delta and input activation 
                #    to get the gradient of the weight.
                # 2. Subtract a ratio (percentage) of the gradient from the weight.
                for i in range(len(self.weights)):
                    layer = np.atleast_2d(inputs[i])
                    error = np.atleast_2d(errors[i])
                    self.weights[i] += learning_rate * layer.T.dot(error)
                # -- Step 4 end
        
    def TanhBackpropagationTraining(self, X, y, learning_rate=0.1, epochs=10000):
        # Add column of ones to X
        # This is to add the bias unit to the input layer
        ones = np.atleast_2d(np.ones(X.shape[0]))
        X    = np.concatenate((ones.T, X), axis=1)
        self.error_rate = []
         
        for k in range(epochs):
            #if k % 100 == 0: print 'epochs:', k
            for i in range(len(X)): # For each training example
                # -- Step 1: Input instance and compute output of every units in the network
                inputs = [X[i]]      # initialize input vectors as [original input vector]   
                for l in range(len(self.weights)):  # l means the position of layer
                    # dot product input vector and weight matrix vector at layer l -> dot_value vector at layer l+1
                    dot_value  = np.dot(inputs[l], self.weights[l])
                    # output vector of units at layer l+1
                    output     = Tanh(dot_value)
                    # output of units at layer l+1 as input of units at layer l+2
                    inputs.append(output)
                # -- Step 1 end.
                
                # -- Step 2 + 3: Calculate error terms in hidden and output units
                # Output layer
                output = inputs[-1]
                errors = [(1-output**2) * (y[i] - output)] # --- tanh function ---
                #errors = [Sigmoid(output) * (1-Sigmoid(output)) * (y[i] - output)] # equivalent to original code
                # Squared error rate: for visualization
                _err   = []
                for tmp in y[i] - output: _err += [tmp * tmp]
                if i == 1: self.error_rate.append(sum(_err) / 2.0)
    
                # Hidden layers: we need to begin at the second to last layer 
                # (a layer before the output layer)
                for l in range(len(inputs) - 2, 0, -1):
                    output = inputs[l]
                    errors.append((1-output**2) * errors[-1].dot(self.weights[l].T))  # --- tanh function ---
                    #errors.append(Sigmoid(output) * (1-Sigmoid(output)) * errors[-1].dot(self.weights[l].T)) # equivalent to original code
    
                # Reverse the order of errors term vectors
                # e.g. [level3(output)->level2(hidden)]  => [level2(hidden)->level3(output)]
                errors.reverse()
                # -- Step 2 + 3 end.
                
                # -- Step 4: Update network weights -> backpropagation
                # 1. Multiply its output delta and input activation 
                #    to get the gradient of the weight.
                # 2. Subtract a ratio (percentage) of the gradient from the weight.
                for i in range(len(self.weights)):
                    layer = np.atleast_2d(inputs[i])
                    error = np.atleast_2d(errors[i])
                    self.weights[i] += learning_rate * layer.T.dot(error)
                # -- Step 4 end    

    def Predict(self, x): 
        output = np.concatenate((np.ones(1).T, np.array(x)), axis=1)      
        for l in range(0, len(self.weights)):
            output = self.activation(np.dot(output, self.weights[l]))
        print 'Output: ', output
        return output
    
    def PrintModel(self):
        print '--- Weights after training ---'
        for l in range(len(self.weights)):
            print '- Layer %i to layer %i: ' % (l, l+1)
            for unit_weight in self.weights[l]: print unit_weight
            
    def EvaluateModel(self, X, y):
        count = 0
        for i, item in enumerate(X):
            nn_output = self.Predict(item)
            if abs(nn_output - y[i]) <= 0.5: count += 1
        print "The accuracy of model: %.2f %%" % (100*count/len(X))
        plt.plot(range(1, len(self.error_rate)+1), self.error_rate, 'b-')
        plt.title('Training Error in Stochastic gradient descent Backpropagation of instance 1')
        plt.xlabel('Iterations')
        plt.ylabel('Error rate')
        plt.axis([1, len(self.error_rate)+1, 0, max(self.error_rate)+1])
        plt.show()

if __name__ == '__main__':
    # Define input and output of data set
    X  = np.array([[0, 0],
                  [0, 1],
                  [1, 0],
                  [1, 1]])
    y  = np.array([0, 1, 1, 0])
    
    # Define the structure of neuron network
    nn1 = NeuralNetwork([2,2,1], 'sigmoid')
    # Training model and evaluate it
    nn1.SigmoidBackpropagationTraining(X, y, 0.1, 10000)
    nn1.PrintModel()
    nn1.Predict([0,0])
    nn1.EvaluateModel(X, y)
    
    
    # Define the structure of neuron network
    nn2 = NeuralNetwork([2,2,1], 'tanh')
    # Training model and evaluate it
    nn2.TanhBackpropagationTraining(X, y, 0.1, 20000)
    nn2.PrintModel()
    nn2.Predict([0,0])
    nn2.EvaluateModel(X, y)