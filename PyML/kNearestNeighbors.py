# -*- coding: utf-8 -*-
'''
@author: Le Vinh
@note: implement K-nearest neighbor From Scratch in Python
       idea programming from http://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/
       For discrete value
@version: 1.0
'''

from __future__ import division
import math, random, operator, collections

''''
@param fname: file name which containing raw data set, and ratio between training and testing set
@requires: input file in format x1, x2, x3, .., label
@return: trainSet and testSet
'''
def LoadData(fname, ratio):
    fin = open(fname, 'r')
    dataset = []
    for line in fin:
        dataset.append(line.replace('\r\n', '').split(','))
    for i, item in enumerate(dataset):
        dataset[i] = map(float, item[:-1]) + [item[-1]]
    random.shuffle(dataset)
    pivot = int(len(dataset)*ratio)
    trainSet = dataset[:pivot]
    testSet  = dataset[pivot:]
    return trainSet, testSet
#LoadData('data/iris.data', 0.67)

''' Compute Euclidean Distance between 2 instances '''
def ComputeDistance(instance1, instance2, length):
    distance = 0
    for i in range(length):
        distance += pow(instance1[i] - instance2[i], 2)
    return math.sqrt(distance)

''' Get k nearest neighbors with instance from training set '''
def GetNeighbors(trainSet, instance, k):
    distances = []
    length = len(instance) - 1
    for i, item in enumerate(trainSet):
        dist = [ComputeDistance(item, instance, length)]
        distances.append((trainSet[i], dist))
    distances.sort(key=operator.itemgetter(1))
    kNeighbors = [inst for (inst, dist) in distances[:k]]
    return kNeighbors

''' Predict output base on majority vote for discrete-value '''
def Predict(neighbors):
    out_list = [item[-1] for item in neighbors]
    counter  = collections.Counter(out_list)
    output   = counter.most_common(1)[0][0]
    #print 'Predicted output: ', output
    return output

''' Evaluate the algorithm '''
def Evaluate(testSet, trainSet, k):
    correct = 0
    for item in testSet:
        output = Predict(GetNeighbors(trainSet, item, k))
        if item[-1] == output: correct += 1
    return (correct/len(testSet)) * 100.0
    
def main():
    trainSet, testSet = LoadData('data/iris.data', 0.67)
    k = 3
    print testSet[1]
    print Predict(GetNeighbors(trainSet, testSet[1], k))
    print 'The correct percentage of %d: %s' % (k, Evaluate(testSet, trainSet, k))
main()