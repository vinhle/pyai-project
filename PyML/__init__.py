'''
@author: Le Vinh
@since: 2016 - JAIST

@keyword: machine learning, classification, prediction methods

@summary:
- Basic concept: FindS
- Decision Tree methods: ID3
- Statistic based methods: Naive Bayes, Gaussian Bayes
- Neuro network methods: Single Perception, Multi-layer Neural Network
- Instance based methods: K Nearest Neighbors, Locally Weigheted Regression
- Kernerl based methods: Support Vector Machine (use scikit-learn)
- Regression: Linear Regression, Logistic Regression, Locally Weigheted Regression
- Genetic methods: (on going)
- Simulation methods: (on going)
- Deep learning: (on going)

@note:
- Algorithms from
+ Machine Learning, Tom Mitchell, 1997
+ Data Mining: Concepts and Techniques, Jiawei Han, 2011
+ Other online tutorials

@status: on going
'''