# -*- coding: utf-8 -*-
'''
@author: Le Vinh
@note: implement Gaussian Naive Bayes From Scratch in Python
       idea from http://machinelearningmastery.com/naive-bayes-classifier-scratch-python/
       For continuous value, and can go over unseen value
@version: 1.0
@weak_point: Gaussian Naive Bayes can accept only integer and float for its formula
            -> Difficult with feature is string
            (possible solution: http://stackoverflow.com/questions/30937667/naivebayes-classifier-handling-different-data-types-in-python)
@strong_point: Gaussian Naive Bayes can deal with unseen feature values
'''

from __future__ import division
import random, math

'''@ -------------------- Data Processing -------------------- @ '''
''''
@param fname: file name which containing raw data set
@requires: input file in format x, y, z, t
@return: nested list
'''
def LoadData(fname):
    fin = open(fname, 'r')
    dataset = []
    for line in fin:
        if ((len(line)>1) & (not line.startswith('@')) & (not line.startswith('%'))):
            dataset.append(line.replace('\n', '').split(','))
    return dataset

''''
@note: split data set into train set and test set by a certain ratio
@param dataset: nested list from LoadData function
@param ratio: division ratio 
@return: pair of train set and test set
'''
def SplitDataByRatio(dataset, ratio):
    trainSize = int(len(dataset) * ratio)
    trainSet  = []
    testSet   = list(dataset)
    while len(trainSet) < trainSize:
        idx = random.randrange(len(testSet))
        trainSet.append(testSet.pop(idx))
    return [trainSet, testSet]
#SplitDataByRatio(LoadData('/data/weather.arff'), 0.8)

''''
@note: split dataset into sets by classes
@param dataset: nested list from SplitDataByRatio function
@return: dictionary {hypothesis: [[instance1_features],[instance2_features]]} 
'''
def SplitDataByClass(dataset):
    classes = {}
    for item in dataset:
        if item[-1] not in classes:
            classes[item[-1]] = []
        classes[item[-1]].append(item[:-1])
    return classes
#SplitDataByClass(LoadData('/data/weather.arff'))
'''@ -------------------- Data Processing -------------------- @ '''


'''@ -------------------- Model Construction -------------------- @ '''
# Compute average or mean of a feature (trung bình | kỳ vọng của một thuộc tính)
# Reference formula: https://en.wikipedia.org/wiki/Variance#Estimating_the_variance
# > Part: Calculating the variance of a fixed set of numbers
def mean(numbers):
    values = [float(value) for value in numbers]
    return sum(values)/float(len(values))
# Compute average or variance of a feature (phương sai của một thuộc tính)
def variance(numbers):
    values = [float(value) for value in numbers]
    avg    = mean(values)
    return sum([pow(x-avg,2) for x in values])/len(values)

'''
@note: construct Naive Bayes Model from train set (Model here is set of means and variances
@param trainSet: from SplitDataByClass
@return: model (means and variances) 
'''
def TrainGaussNaiveBayesModel(trainSet):
    classes    = {}
    means      = {}
    variances  = {}
    model      = (classes, means, variances)
    for clas in trainSet:
        classes[clas] = len(trainSet[clas])
        means[clas] = [mean(feature) for feature in zip(*trainSet[clas])]
        variances[clas] = [variance(feature) for feature in zip(*trainSet[clas])]
    return model
#TrainGaussNaiveBayesModel(SplitDataByClass(LoadData('data/pima-indians-diabetes.arff')))
'''@ -------------------- Model Construction -------------------- @ '''

'''@ -------------------- Model Evaluation -------------------- @ '''
# Reference: http://www.cs.cmu.edu/~epxing/Class/10701-10s/Lecture/lecture5.pdf
def ProbGaussFormula(x, mean, variance):
    return (1 / (math.sqrt(2*math.pi * variance))) * math.exp(-(math.pow(x-mean,2) / (2*variance)))

'''
@note: predict class a new instance by Naive Bayes formula (traditional for discrete value)
@param model: Bayes model from TrainNaiveBayesModel
@param instance: list values of new instance
@attention: there is a problem here with a new feature (not appear in training) then P(new_featr|h) = 0
'''
def PredictInstanceGaussNaiveBayes(model, instance):
    classes     = model[0]
    means       = model[1]
    variances   = model[2]
    # Compute probability of hypothesis h = P(h).PGaus(feat1)....PGaus(featn)
    hypothesis = {}
    for clas in classes:
        target_funct = classes[clas]
        for idx, attr in enumerate(instance):
            target_funct *= ProbGaussFormula(attr, means[clas][idx], variances[clas][idx])
        hypothesis[clas] = target_funct
    for clas in hypothesis: print 'Probability of \'%s\': %r' % (clas, hypothesis[clas])
    
    # Result normalization
    normal = {}
    for clas in hypothesis:
        normal[clas] = hypothesis[clas] / sum([val for val in hypothesis.values()])
    for clas in normal: print 'Probability of \'%s\' after normalizing: %.3f ' % (clas, normal[clas])
    
    # Decision making
    print '----- Decesion making -----'
    max_target_value = (max([val for val in normal.values()]))
    max_hypothesis = [clas for clas, val in normal.items() if val == max_target_value][0]
    prediction = 'Instance with '
    for feature in instance:
        prediction += str(feature) + ' + '
    prediction = prediction[:-2] + ' -> ' + str(max_hypothesis) + ' in ' + "%.3f" % max_target_value + ' %'
    print prediction
    return max_hypothesis
    
'''
@note: predict class a new instance by Naive Bayes formula
@param model: Bayes model from TrainNaiveBayesModel
@param instance: list values of new instance 
'''
def EvaluateModel(model, testset):
    testSize    = sum([len(tmp) for tmp in testset.values()])
    num_correct = 0
    for clas in testset:
        for item in testset[clas]:
            item = [float(v) for v in item]
            if clas == PredictInstanceGaussNaiveBayes(model, item):
                num_correct += 1
    print "Evaluation: %.3f %% correct" % (100*num_correct / testSize)
    return (num_correct / testSize)
'''@ -------------------- Model Evaluation -------------------- @ '''
    
# Test program
def main():
    dataset = LoadData('data/pima-indians-diabetes.arff')
    trainset, testset = SplitDataByRatio(dataset, 0.8)
    trainset = SplitDataByClass(trainset)
    testset  = SplitDataByClass(testset)
    model    = TrainGaussNaiveBayesModel(trainset)    
    
    instance = [8,183,64,0,0,23.3,0.672,32]
    PredictInstanceGaussNaiveBayes(model, instance)
    EvaluateModel(model, testset)
main()