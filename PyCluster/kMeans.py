# -*- coding: utf-8 -*-
'''
@author: Le Vinh
@note: implement k-Means
@note: Chapter 10: Cluster Analysis: Basic Concepts and Methods (Data Mining Concepts and Techniques)
@version: 1.0
'''

import math, random, collections

class kMeans:
    '''
    k: the number of cluster
    fname: the name of data file
    centroids: # list of vector controid
    clusters:  # list of cluster (list of input key object) 
    '''
    def __init__(self, fname, k):
        self.k = k
        self.feature_len, self.objects, self.outputs = self.LoadData(fname)        
        self.centroids, self.clusters = self.InitialCentroids(self.objects, self.k)
        
    def LoadData(self, fname):
        fin     = open(fname, 'r')
        dataset = [line.replace('\n', '').split(',') for line in fin]
        objects = {}
        outputs = {}
        for i,inst in enumerate(dataset):
            objects[i] = map(float, inst[:-1])  # Map of feature input (key = index)
            outputs[i] = inst[-1].strip()       # Map of label
        feature_len = len(objects[0])
        return feature_len, objects, outputs
    
    # Random choose k objects from D as the initial centers
    def InitialCentroids(self, objects, k):
        object_idxs = random.sample(range(0, len(objects)), k)
        centroids = [objects[idx] for idx in object_idxs]
        cluster = [[object_idx] for object_idx in object_idxs]    # Assign each centroid to an cluster
        print 'Initial centroids', centroids
        print 'Initial cluster ', cluster
        return centroids, cluster         
        
    # Compute the distance between 2 objects
    def ComputeDistance(self, v1, v2):
        dist = sum( [math.pow(v1[i]-v2[i], 2) for i in range(len(v1))] )
        return math.sqrt(dist)
    
    # Recompute centroids (a mean vector of a cluster)
    def ComputeMeans(self):
        k = 0
        centroids = [None] * self.k
        for cluster in self.clusters:
            #print cluster
            for i in range(self.feature_len):                
                tmp = [self.objects[object_idx][i] for object_idx in cluster]
                if centroids[k] == None:
                    centroids[k] = [sum(tmp)/len(tmp)]
                else:
                    centroids[k].append(sum(tmp)/len(tmp))
            k += 1
        return centroids
    
    # Find cluster for a object (min distance with cluster's mean)
    def FindCluster(self, obj):
        dist = []
        for centroid in self.centroids:
            dist += [self.ComputeDistance(centroid, obj)]
        cluster_idx = dist.index(min(dist))
        return cluster_idx 
    
    # Main algorithm
    def Cluster(self):
        isUpdate = True
        count    = 0
        while isUpdate:
            isUpdate = False
            print 'Iteration', count
            # (re)assign objects to clusters
            for object_idx in self.objects:
                cluster_idx = self.FindCluster(self.objects[object_idx])
                #print '\tobject[' + str(object_idx) + ']=' + str(self.objects[object_idx]) + ' in cluster '+ str(cluster_idx) + ' have centroid ' + str(self.centroids[cluster_idx])
                for j in range(len(self.clusters)):
                    if j == cluster_idx:
                        if self.clusters[j] == None: self.clusters[j] = [object_idx]
                        else:
                            if not object_idx in self.clusters[j]:
                                self.clusters[j].append(object_idx)
                                isUpdate = True
                    else: # j != cluster_idx
                        if self.clusters[j] != None:
                            if object_idx in self.clusters[j]:
                                self.clusters[j].remove(object_idx)
                print '\tClusters:', self.clusters
            # update clusters mean
            self.centroids = self.ComputeMeans()
            count         += 1
    
    # Print Model
    def PrintModel(self):
        for i, cluster in enumerate(self.clusters):
            print '\nCluster %d has %d objects' % (i, len(cluster))
            output = []
            for j in cluster:
                output.append(self.outputs[j])
                print str(self.objects[j]) + ':' + self.outputs[j],
            df = collections.Counter(output)
            print '\nDistribution frequency:', df


def main():            
    #classifer = kMeans("data/kmeans_test.data", 2)
    classifer = kMeans("data/iris.data", 3)
    classifer.Cluster()
    print '\nCluster result:',
    classifer.PrintModel()
main()