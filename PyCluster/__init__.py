'''
@author: Le Vinh
@since: 2016 - JAIST

@keyword: unsupervised machine learning, clustering, ranking methods

@summary:
- Partioning cluster: k-Means
- Hierarchical cluster: (on going)
- Density based cluster: (on going)
- Grid based cluster: (on going)
- Ranking algorithms: (on going)

@note:
- Algorithms from
+ Data Mining: Concepts and Techniques, Jiawei Han, 2011
+ Other online tutorials

@status: on going
'''