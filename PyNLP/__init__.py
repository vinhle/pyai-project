'''
@author: Le Vinh
@since: 2016 - JAIST

@keyword: natural language processing, NLTK toolkit, keywords extraction

@summary:
- Basic NLP algorithms
+ Syntactic parser: Chart, CKY algorithms
+ Morphological Analysis: POS Tagging(Hidden Markov Model & Viterbi)

- NLTK Toolkit book
1. Language Processing and Python
2. Accessing Text Corpora and Lexical Resources
3. Processing Raw Text
4. Writing Structured Programs
5. Categorizing and Tagging Words
6. Learning to Classify Text
7. Extracting Information from Text
8. Analyzing Sentence Structure
9. Building Feature Based Grammars
10. Analyzing the Meaning of Sentences

- Keywords extraction
+ TF-IDF
+ TextRank

@note:
- Algorithms from
+ Language Processing Lecture, Prof. Minh Le Nguyen, Japan Advanced Institute of Science and Technology
+ NLTK toolkit book: http://www.nltk.org/book/
+ Other online tutorials, papers

@status: on going
'''