""" LE VINH - 7/2015
Morphological Analysis - POS Tagging Task
Hidden Markov Model and Viterbi Algorithm - base on lecture 08 NLP1"""
class Node:
    """Define an class Node: word, POS of word, probability of word at a POS, local best probability"""
    def __init__(self, word, pos, node_prob, best_prob):
        self.word   = word
        self.pos    = pos
        self.node_prob = node_prob
        self.best_prob = best_prob
    #  Return a printable representation of the object
    def __repr__(self):
        return self.word + ' / ' + self.pos + ' / ' + str(self.node_prob) + ' / ' + str(self.best_prob[0])


def load_grammar(fin):
    """ Load probability from file into a 2 dictionaries
    Return: two dictionaries for probability (Hidden Markov Model) """
    prob_word = {} # Dictionary for probability of output of symbol at a POS {'word':{'POS':probability}}
    prob_pos  = {} # Dictionary for probability of POS transition {'next_POS' : {'current_POS':probability}}
    flag = 0
    for line in fin:
        line = line.split()
        if (len(line)>0) and (line[0]=='#'):
            flag += 1
            continue                
        if flag == 1:
            if (len(line)>0) and (line[0]!='#'): # Not empty and comment line
                prob = float(line[0])
                word = line[1]
                pos  = line[2]
                if(word in prob_word):                
                    prob_word[word][pos] = prob
                else:
                    prob_word[word] = {pos:prob}
        if flag == 2:
            if (len(line)>0) and (line[0]!='#'): # Not empty and comment line
                prob = float(line[0])
                nxt  = line[1]
                curr = line[2]
                if(nxt in prob_pos):                
                    prob_pos[nxt][curr] = prob
                else:
                    prob_pos[nxt] = {curr:prob}

    print "- Probability of output of symbol at a POS:"
    for word in prob_word:
        for pos in prob_word[word]:
            print 'P(' + word + '|' + pos + ') = ' + str(prob_word[word][pos])
    print "- Probability of POS transition:"
    for nxt in prob_pos:
        for curr in prob_pos[nxt]:
            print 'P(' + nxt + '|' + curr + ') = ' + str(prob_pos[nxt][curr])
            
    return prob_word, prob_pos

def viterbi(sentence, prob_word, prob_pos):
    """ Implementing Viterbi algorithm """
    words  = ['START'] + sentence.split()    
    length = len(words)
    list_nodes = [[] for x in range(length)] # list of list word node in each layer
    list_nodes[0] = [Node('START', 'START', 1, [1,0])]
    for i in range(1,length): # loop in sequence of words
        for pos in prob_word[words[i]]: # loop in type of word
            word = words[i]
            node_prob = prob_word[word][pos]
            curr_prob = []
            for pre_node in list_nodes[i-1]: # loop in previous node layer to find all probability
                tmp = pre_node.best_prob[0] * prob_pos[pos][pre_node.pos] * node_prob
                curr_prob.append([tmp, list_nodes[i-1].index(pre_node)])
            best_prob = curr_prob[0] # best_prob is a list [best probability, index of parent node in previous layer 
            for tmp in curr_prob: # find the largest probability of node
                if best_prob[0] < tmp[0]:
                    best_prob = tmp
            node = Node(word, pos, node_prob, best_prob)
            list_nodes[i].append(node)                   
                                  
    print "- List of nodes: "
    for i in range(length):
        print "Layer", i
        for node in list_nodes[i]:
            print node
    
    print "- Result of POS tagging for the sentence: ", sentence
    final_best = list_nodes[length-1][0]    
    for tmp in list_nodes[length-1]: # find the largest probability of final node
        if final_best.best_prob[0] < tmp.best_prob[0]: final_best = tmp
    i = length-1
    j = list_nodes[length-1].index(final_best)
    path = []
    while(i>0):
        path.append(list_nodes[i][j])     
        i = i - 1
        j = list_nodes[i][j].best_prob[1]
    path.reverse()
    for node in path:
        print node.word + '/' + node.pos,
    
# Run program
if __name__ == '__main__':
    print "Morphological Analysis - POS Tagging Task - Implement Viterbi Algorithm - base on lecture 08 NLP1"
    print "Choice 1: Try with data example 01"
    print "Choice 2: Try with your data"
    print "Choice 3: Quit the program"
        
    while True:
        choice = raw_input("\n* Your choice: ")        
        if choice == "1":
            datafile = open('data/posData01', 'r')
            sentence = "young boys like red caps"
            prob_word, prob_pos = load_grammar(datafile)
            viterbi(sentence, prob_word, prob_pos)
        else:
            if choice == "2":
                fileName = raw_input("* Your data file: ")
                sentence = raw_input("* Your sentence: ")
                try:
                    datafile = open(fileName, 'r');
                    prob_word, prob_pos = load_grammar(datafile)
                    viterbi(sentence, prob_word, prob_pos)
                except:
                    print "No source data file or error sentence"
                    continue
            else:
                if choice == "3": print "Thank you, bye bye"; break
                else:
                    print "Your choice is incorrect format! Please input again."
    print "Finish"