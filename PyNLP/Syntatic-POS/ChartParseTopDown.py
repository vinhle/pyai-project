""" LE VINH - 7/2015
Syntactic Parsing Task
Implement Top-down Chart algorithms by stack - base on lecture 04 NLP1"""

class Arc:
    count = 0
    """ Define an class Arc: id of arc, start point(i), end point(j), grammar rule, completed so far, previous rule """
    def __init__(self, i, j, rule, completed, prev):
        Arc.count += 1
        self.id = Arc.count        # type int
        self.i = i                  # type int
        self.j = j                  # type int
        self.rule = rule            # type Rule
        self.completed = completed  # type list    
        self.prev = prev            # type list
    # Compare two object, used in sorted list
    def __eq__(self, other):
        return self.i == other.i and self.j == other.j and self.rule == other.rule and self.completed == other.completed and self.prev == other.prev

    def isInactive(self):
        return (len(self.completed) == len(self.rule.rhs))    
    def nextSymbol(self):
        if self.isInactive(): return ''
        else:
            return self.rule.rhs[len(self.completed)]

class Rule:
    """ Define an class Rule: symbols in left hand side and right hand side """
    def __init__(self, lhs, rhs):
        self.lhs = lhs  # type string
        self.rhs = rhs  # list of string
    #  Return a printable representation of the object
    def __repr__(self):
        return str(self.lhs) + ' -> ' + str(self.rhs)
    
def inHistory(history, arc):
    """ Check an arc in history or not"""
    for tmp in history:
        if tmp == arc:
            Arc.count = Arc.count - 1
            return True
    return False

def load_grammar(fin):
    """ Load grammar from file into 2 lists, first for phrase structured rules, second for lexical rules """
    grammar = []
    lexicon = []
    flag = 0
    for line in fin:
        line = line.split()
        if (len(line) > 0): # Not empty
            if (line[0] != '#'):
                if len(line) == 1:
                    initial = line[0]
                else:
                    if(flag == 1): # lexical rules
                        rule = Rule(line[0], line[1:])
                        grammar.append(rule)                    
                    if(flag == 2):  # phrase structured rules
                        rule = Rule(line[0], [line[1]])
                        lexicon.append(rule)
            else: flag += 1
    print "- Phrase structured rules:"
    for rule in grammar: print rule.lhs + ' -> ' + ' '.join(rule.rhs)
    print "- Lexical rule"
    for rule in lexicon: print rule.lhs + ' -> ' + ' '.join(rule.rhs)
    return grammar, lexicon, initial
    
def chart_parse(sentence, grammar, lexicon, initial):
    """ Implementing Top-down Chart parsing algorithm """
    words  = sentence.split()
    length = len(words)

    # Step 1: Initialization of agenda
    # - For all word in an input sentence, create inactive arc
    agenda = [] # chart is a list of arcs
    history = []
    for i in range(length):
        flag = 0
        for j in range(len(lexicon)):
            if lexicon[j].rhs[0] == words[i]:
                newarc = Arc(i, i+1, lexicon[j], lexicon[j].rhs, [])
                agenda.append(newarc)
                history.append(newarc)
                flag = 1
        if(flag == 0):
            print 'Error: word ' + words[i] + ' is not in grammar. No possible parse tree'
            return None
    # - Pushing initial arcs
    for rule in grammar:
        if rule.lhs == initial:
            newarc = Arc(0, 0, rule, [], [])
            agenda.append(newarc)
            history.append(newarc)
    #print agenda
    
    # Step 2-3: Popping of arc and creation of new arcs
    chart = []
    while(len(agenda) != 0):
        tmp = agenda.pop()  # stack FILO
        chart.append(tmp)
        
        # If there is the same arc in history, do nothing
        # Create new arc and push to agenda
        if(tmp.isInactive()):
            # Concatenation and no Proposal
            for arc in chart:
                if arc.nextSymbol() == tmp.rule.lhs and arc.j == tmp.i:
                    newarc = Arc(arc.i, tmp.j, arc.rule, arc.completed + [tmp.rule.lhs], arc.prev + [tmp.id])
                    if inHistory(history, newarc) == False:
                        agenda.append(newarc)
                        history.append(newarc)
        else:
            for arc in chart: # Concatenation
                if arc.isInactive() and tmp.nextSymbol() == arc.rule.lhs and tmp.j == arc.i:
                    newarc = Arc(tmp.i, arc.j, tmp.rule, tmp.completed + [arc.rule.lhs], tmp.prev + [arc.id])
                    if inHistory(history, newarc) == False:
                        agenda.append(newarc)
                        history.append(newarc)
            for rule in grammar: # Proposal    
                if tmp.nextSymbol() == rule.lhs:
                    newarc = Arc(tmp.j, tmp.j, rule, [], [])
                    if inHistory(history, newarc) == False:
                        agenda.append(newarc)
                        history.append(newarc)
                    
    print "- List of arcs: "
    for arc in chart:
        tmp = str(arc.id) + ':[' + str(arc.i) + ', ' + str(arc.j) + ', ' + arc.rule.lhs + ' -> '
        k = 0
        for sym in arc.completed:
            if(len(arc.prev) != 0):
                tmp += sym + '(' + str(arc.prev[k]) + ') '
                k   += 1
            else: tmp += sym + ' '
        tmp += '. '
        for i in arc.rule.rhs[len(arc.completed):]: tmp += i + ' '
        tmp += ']'
        print tmp
    return chart

def print_ptree(chart, sentence, initial):
    """Print parser tree for the sentence"""
    words  = sentence.split()    
    length = len(words)
    numPar = [] # the number of parse tree   
    for arc in chart:
        if arc.rule.lhs == initial and arc.isInactive() and arc.j == length:
            numPar.append(arc) 
    if(len(numPar) != 0):
        print "- We have " + str(len(numPar)) + " parser tree for the sentence: " + sentence
        for i in range(len(numPar)):
            print " + Parser tree ", str(i+1)
            prt = parser_tree(chart, numPar[i].id)
            print prt.replace(" )", ")")
    else:
        print "This sentence is wrong in grammar, there is no parse tree."
def parser_tree(chart, root):
    for tmp in chart:
        if tmp.id == root:
            arc = tmp
            break
    ptree = ''
    if(len(arc.prev) != 0):
        ptree = "(" + arc.rule.lhs + " "
        for i in range(len(arc.prev)): ptree += parser_tree(chart, arc.prev[i]) 
        ptree += ") "
    else:
        ptree = "(" + arc.rule.lhs + " " + arc.rule.rhs[0] + ") "
    return ptree

# Run program
if __name__ == '__main__':
    print "Syntactic Parsing Task - Implement Top-down Chart algorithms by stack - base on lecture 04 NLP1"
    print "Choice 1: Try with grammar-sentence example 01"
    print "Choice 2: Try with grammar-sentence example 02"
    print "Choice 3: Try with your grammar-sentence"
    print "Choice 4: Quit the program"
        
    while True:
        choice = raw_input("* Your choice: ")        
        if choice == "1":
            gramfile = open('data/chartData01', 'r');
            grammar, lexicon, initial = load_grammar(gramfile)
            sentence = 'the cup broke'
            chart = chart_parse(sentence, grammar, lexicon, initial)
            if(chart != None):
                print_ptree(chart, sentence, initial)
            Arc.count = 0
        else:
            if choice == "2":
                gramfile = open('data/chartData02', 'r');
                grammar, lexicon, initial = load_grammar(gramfile)
                sentence = 'red horse meat'
                chart = chart_parse(sentence, grammar, lexicon, initial)
                if(chart != None):
                    print_ptree(chart, sentence, initial)
                Arc.count = 0
            else:
                if choice == "3":
                    fileName = raw_input("* Your grammar file: ")
                    sentence = raw_input("* Your sentence: ")
                    try:
                        gramfile = open(fileName, 'r');
                        grammar, lexicon, initial = load_grammar(gramfile)
                        chart = chart_parse(sentence, grammar, lexicon, initial)
                        if(chart != None):
                            print_ptree(chart, sentence, initial)
                        Arc.count = 0
                    except:
                        print "No such file:", fileName
                        continue
                else:
                    if choice == "4": print "Thank you, bye bye"; break
                    else:
                        print "Your choice is incorrect format! Please input again."
    print "Finish"
