""" LE VINH - 7/2015
Syntactic Parsing Task
Implement CKY algorithms - base on lecture 04 NLP1"""

import copy

class Cell:
    """Define an class cell: Non-terminal symbol(left hand side), probability of rule, position of symbol right1 and right2"""
    def __init__(self, left, pro, right1, right2):
        self.left   = left
        self.pro    = pro
        self.right1 = right1 # tuple has 3 elements to identify position row, col, case in cell
        self.right2 = right2 # tuple has 3 elements to identify position row, col, case in cell
    def __copy__(self):
        #sprint '__copy__()'
        return Cell(self.left, self.pro, self.right1, self.right2)
      
      
def load_grammar(fin):
    """ Load grammar from file into a dictionary, key is the right hand side and value is [the left hand side, probability] of rule
    This storing grammar is suitable for bottom-up manner algorithms
    Return: a dictionary for grammar """
    gram_dict = {}
    for line in fin:
        line = line.split()
        if (len(line)>0) and (line[0]!='#'): # Not empty and comment line
            key = ''
            prob  = float(line[0])
            value = [prob, line[1]]            
            for symbol in line[2:]:
                key = key + ' ' + symbol            
            gram_dict[key[1:]] = value
    print "- Grammar:"
    for rule in gram_dict:
        print gram_dict[rule][1] + ' -> ' + rule
    return gram_dict

def CKY_parse(sentence, grammar):
    """ Implementing CKY algorithm 
    Return: a table - a 3-dimensions list """
    words  = sentence.split()    
    length = len(words)
    
    # Initial table as a matrix length * length, each elements is a empty list
    table = [[[] for y in range(length)] for x in range(length)]
    
    # Applying lexical rules
    for i in range(0, length):
        sym = words[i]
        if(sym in grammar):
            table[i][i].append(Cell(grammar[sym][1], grammar[sym][0], sym, None));
        else:
            print "This sentence contains strange words which are not in dictionary:", sym
            return None
    #print table
    
    # Phrase structured Rules
    for d in range(1, length):       # loop in diagonalization
        for i in range(0, length-d): # loop in row
            j = i + d
            for k in range(i, j):    # loop to compute a new cell
                if (len(table[i][k])!=0 and len(table[k+1][j])!=0):
                    for c1 in range(0, len(table[i][k])):   # loop inside a left-cell
                        for c2 in range(0, len(table[k+1][j])): # loop inside a down-cell
                            rule = table[i][k][c1].left + ' ' + table[k+1][j][c2].left
                            if(rule in grammar):
                                prob = grammar[rule][0] * table[i][k][c1].pro * table[k+1][j][c2].pro
                                #table[i][j].append(Cell(grammar[rule][1], grammar[rule][0], (i,k,c1), (k+1,j,c2)))
                                table[i][j].append(Cell(grammar[rule][1], prob, (i,k,c1), (k+1,j,c2)))
    """for line in table:
        for e in line:
            if(len(e)==0): print '[]',
            else: print e[0].left,
        print"""
    return table

def print_ptree(table, sentence):
    """Print parser tree for the sentence"""
    words  = sentence.split()    
    length = len(words)
    numPar = len(table[0][length-1]) # the number of parse tree    
    if(len(table[0][length-1]) != 0):
        print "- We have " + str(len(table[0][length-1])) + " parser tree for the sentence: " + sentence
        for i in range(numPar):
            #print " + Parser tree " + str(i+1) + " has probability " + str(table[0][length-1][i].pro)
            print " + Parser tree " + str(i+1)
            prt = parser_tree(table, 0, length-1, i)
            print prt.replace(" )", ")")
    else:
        print "This sentence is wrong in grammar, there is no parse tree."
def parser_tree(table, row, col, case):
    cel = copy.copy(table[row][col][case])
    ptree = ''
    if(isinstance(cel.right2, tuple)):
        ptree = "(" + cel.left + " " + parser_tree(table, cel.right1[0], cel.right1[1], cel.right1[2]) + parser_tree(table, cel.right2[0], cel.right2[1], cel.right2[2]) + ") "
    else:
        ptree = "(" + cel.left + " " + cel.right1 + ") "
    return ptree

# Run program
if __name__ == '__main__':
    print "Syntactic Parsing Task - Implement CKY algorithms - base on lecture 04 NLP1"
    print "Choice 1: Try with grammar-sentence example 01"
    print "Choice 2: Try with grammar-sentence example 02"
    print "Choice 3: Try with your grammar-sentence"
    print "Choice 4: Quit the program"
        
    while True:
        choice = raw_input("* Your choice: ")        
        if choice == "1":
            gramfile = open('data/ckyData01', 'r');
            grammar  = load_grammar(gramfile)
            sentence = 'the man broke a desk with a drawer'
            table    = CKY_parse(sentence, grammar)
            if(table != None):
                print_ptree(table, sentence)
        else:
            if choice == "2":
                gramfile = open('data/ckyData02', 'r');
                grammar  = load_grammar(gramfile)
                sentence = 'Mary eats cakes with cream'
                table    = CKY_parse(sentence, grammar)
                if(table != None):
                    print_ptree(table, sentence)
            else:
                if choice == "3":
                    fileName = raw_input("* Your grammar file: ")
                    sentence = raw_input("* Your sentence: ")
                    try:
                        gramfile = open(fileName, 'r');
                        grammar  = load_grammar(gramfile)
                        table    = CKY_parse(sentence, grammar)
                        if(table != None):
                            print_ptree(table, sentence)
                    except:
                        print "No such file:", fileName
                        continue                    
                else:
                    if choice == "4": print "Thank you, bye bye"; break
                    else:
                        print "Your choice is incorrect format! Please input again."
    print "Finish"
