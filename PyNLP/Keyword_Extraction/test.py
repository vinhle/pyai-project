import TfIdf, TextRank


print "Keyword extraction from 3 documents."
print "1. Implement TF-IDF by NLTK and pure Python"
TfIdf.TfIdf_Imp1()
print "2. Implement TF-IDF by Scikit-Learn"
TfIdf.TfIdf_Imp2()
print "3. Implement TextRank by NLTK and Networkx"
TextRank.TextRank_Imp()