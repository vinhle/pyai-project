""" Implement TF-IDF:
    - Computes IDF for a specified term based on the corpus
    - Generates keywords ordered by tf-idf for a specified document """
# -*- coding: utf-8 -*-
from __future__ import division
import math, string, nltk
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer, LancasterStemmer, WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np

""" Pre-processing raw collection of data """
def preprocess(doc):
    # Convert to lower case
    txt = doc.lower()
    
    # Tokenization by NLTK
    tokens = nltk.word_tokenize(txt, language='english')
    
    # Punctuation and Number Removal
    tokens = [w for w in tokens if w not in string.punctuation]
    tokens = [w for w in tokens if not w.isdigit()]
    
    # Stop Word Removal
    content_tokens = [w for w in tokens if w not in stopwords.words('english')]
    
    # Stemming using NLTK - Porter Stemmer: http://textminingonline.com/dive-into-nltk-part-iv-stemming-and-lemmatization
    # Base on: Porter | Lancaster | Snowball Stemming Algorithm | Lemmatization based on WordNet's
    # stemmer = PorterStemmer() | LancasterStemmer() | SnowballStemmer() | LancasterStemmer()
    # stemmed = [stemmer.stem(w) for w in content_tokens]
    lemmatizer = WordNetLemmatizer()
    lemmatized = [lemmatizer.lemmatize(w) for w in content_tokens]
    
    #fdist  = nltk.FreqDist(lemmatized)
    #print fdist.most_common(4)
    return lemmatized
        

""" Version 1: Computer TF-IDF weight by Scikit-Learn """
def tfidf_sklearn(fname):
    fin = open('doclist.txt', 'r')
    doclist = [doc for doc in fin]
    # print doclist
    # ngram_range : generate 2 and 3 word phrases
    # vector_model = TfidfVectorizer(analyzer='word', ngram_range=(1,3), min_df = 0, stop_words = 'english')
    vector_model  = TfidfVectorizer(analyzer='word', min_df = 0, stop_words = 'english')
    
    sparse_matrix = vector_model.fit_transform(doclist)
    dense_matrix  = sparse_matrix.todense() # matrix describe vector model of document corpus
    #print 'Vectorization of each document in corpus', dense_matrix
    
    features = vector_model.get_feature_names()
    #print 'Feature names: ', features    
    
    # Find the index of n largest elements in a list or np.array
    count = 1
    for vector in dense_matrix:
        vec = np.array(vector)[0]        # convert from matrix to array in numpy    
        index = vec.argsort()[-3:][::-1] # list index of largest elements
        #print "Three top words in document ", count
        count += 1
        for i in index:
            #print "\tWord: %s, TF-IDF: %f" % (features[i], round(vec[i], 5))
            print features[i] + ', ',
        print
    
    #doc   = "Python is a kind of snake."
    #print "New document: ", vector_model.transform(doc)
    
""" Version 2: Computer TF-IDF weight implement"""
# Term Frequency in a specific document
def tf(word, doc): return doc.count(word)/len(doc)

# The number of documents contains a specific term
def n_containing(word, doclist): return sum(1 for doc in doclist if word in doc)

# Inverse Document Frequency in a specific document
def idf(word, doclist): return math.log(len(doclist) / n_containing(word, doclist))

# Compute TF-IDF score. 
def tfidf(word, doc, doclist): return tf(word, doc) * idf(word, doclist)

""" Main program"""
def TfIdf_Imp1():
    fin     = open('doclist.txt', 'r') # each line is a document
    count   = 1
    doclist = []
    for line in fin:
        document = preprocess(line) # list of cleaned words
        doclist.append(document)
    for doc in doclist:
        #print "Three top words in document %d" % count
        count += 1
        score_words = {word: tfidf(word, doc, doclist) for word in doc}    
        key_words   = sorted(score_words.items(), key=lambda x: x[1], reverse=True)
        for word, score in key_words[:3]:
            #print "\tWord: %s, TF-IDF: %f" % (word, round(score, 5))
            print word + ', ',
        print 

def TfIdf_Imp2():
    tfidf_sklearn('doclist.txt')
    