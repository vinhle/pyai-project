# -*- coding: utf-8 -*-
import re, math

'''
@note: Compute similarity between two sentences
'''
# compute the similarity of two sentences
# http://stackoverflow.com/questions/15173225/how-to-calculate-cosine-similarity-given-2-sentence-strings-python

WORD = re.compile(r'\w+')

def Text2Vector(sent):
    words  = WORD.findall(sent)
    vector = [0,0,0]
    start  = 0
    end    = 0
    for w in words:
        if (w=='size') or (w=='length'):
            vector[0] = 1
            start = words.index(w)
        if (w=='buffer') or (w=='memory') or (w=='string') or (w=='array'):
            vector[2] = 1
            end = words.index(w)
    if 'of' in words[start:end]:
        vector[1] = 1
    return vector
        
def GetCosine(vec1, vec2):
    vec_product = sum([vec1[x] * vec2[x] for x in [0,1,2]])
    sum_sqrt1   = sum([x**2 for x in vec1])
    sum_sqrt2   = sum([x**2 for x in vec2])
    denominator = math.sqrt(sum_sqrt1) * math.sqrt(sum_sqrt2)

    if not denominator: return 0.0
    else:
        return round(float(vec_product)/denominator, 3)

def test():
    # base_vect = ['size', 'of', 'buffer'], and size = length, buffer = memory = string
    base_vect = [1, 1, 1]
    text1 = 'The size of the buffer'
    text2 = 'The length of a volume name buffer, in TCHARs'
    text3 = 'The size of a volume name buffer, in TCHARs'
    text4 = 'A pointer to a buffer that receives the computer name or the cluster virtual server name. '
    
    #get_cosine(text_to_vector(text1),text_to_vector(text2))
    
    vector1 = Text2Vector(text1)
    print vector1
    vector2 = Text2Vector(text2)
    print vector2
    vector3 = Text2Vector(text3)
    print vector3
    vector4 = Text2Vector(text4)
    print vector4
    
    cosine = GetCosine(base_vect, vector2)
    print 'Cosine:', cosine
    cosine = GetCosine(base_vect, vector3)
    print 'Cosine:', cosine
    cosine = GetCosine(base_vect, vector4)
    print 'Cosine:', cosine
#test()