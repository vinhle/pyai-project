""" --- Chapter 6. Learning to Classify Text ---
    How can we identify particular features of language data that are salient for classifying it?
    How can we construct models of language that can be used to perform language processing tasks automatically?
    What can we learn about language from these models?
"""
import nltk, random

""" Gender Identification """
def GenderCorpus():
    from nltk.corpus import names
    labeled_names = ([(name, 'male') for name in names.words('male.txt')]
                     + [(name, 'female') for name in names.words('female.txt')])
    random.shuffle(labeled_names) # lam hon loan vi tri cac phan tu trong mang
    return labeled_names

# define feature extractor -> features dictionary: {feature_name, feature_value}
def FeatureExtractor(name):
    return {'last_letter': name[-1]}
def FeatureExtractor2(name):
    #features = {}
    #features['suffix1'] = name[-1:]
    #features['suffix2'] = name[-2:]
    #return features
    return {'suffix1': name[-1:], 'suffix2': name[-2:]}
def GenderClassifierModel():
    # prepare training - development - testing data
    labeled_names   = GenderCorpus()
    train_names     = labeled_names[1500:]
    devtest_names   = labeled_names[500:1500]
    test_names      = labeled_names[:500]
    train_set       = [(FeatureExtractor2(n), gender) for (n, gender) in train_names]
    test_set        = [(FeatureExtractor2(n), gender) for (n, gender) in test_names]
    
    # learning algorithm (naive Bayes) -> classifier model
    classifier = nltk.NaiveBayesClassifier.train(train_set)

    # predict unseen data
    name = 'Neo'
    print classifier.classify(FeatureExtractor2('Neo'))

    # error analysis
    errors = []
    for (name, tag) in devtest_names:
        print name
        guess = classifier.classify(FeatureExtractor2(name))
        if guess != tag:
            errors.append( (tag, guess, name) )
    for (tag, guess, name) in sorted(errors):
        print('correct={:<8} guess={:<8s} name={:<30}'.format(tag, guess, name))
    
    # evaluate the classifier & determine the most effective features
    print(nltk.classify.accuracy(classifier, test_set))
    print classifier.show_most_informative_features(5)

#GenderClassifierModel()

""" Part-of-Speech Tagging by suffixes regular expression tagger """
from nltk.corpus import brown
# Find out what the most common suffixes
def common_suffixes():
    suffix_fdist = nltk.FreqDist()
    for word in brown.words():
        word = word.lower()
        suffix_fdist[word[-1:]] += 1
        suffix_fdist[word[-2:]] += 1
        suffix_fdist[word[-3:]] += 1
    common_suffixes = [suffix for (suffix,count) in suffix_fdist.most_common(100)]
    return common_suffixes
# Compute features of a word
def pos_features(word, common_suffixes):
    features = {}    
    for suffix in common_suffixes:
        features['endswith({})'.format(suffix)] = word.lower().endswith(suffix)
    return features
# Construct and evaluate the model
def POSModel():
    tagged_words = brown.tagged_words(categories='news')
    most_suffixes = common_suffixes()
    features_set = [(pos_features(word, most_suffixes), tag) for (word, tag) in tagged_words]
    size = int(len(features_set) * 0.1)
    train_set, test_set = features_set[size:], features_set[:size]
    
    classifier = nltk.DecisionTreeClassifier.train(train_set)
    print "Accuracy: ", nltk.accuracy(classifier, test_set)
    print "Word 'cats': ", classifier.classify(pos_features('cats', most_suffixes))
    print(classifier.pseudocode(depth=4))
#POSModel()

""" Decision Tree """
import math
def entropy(labels):
    freqdist = nltk.FreqDist(labels)
    probs    = [freqdist.freq(l) for l in freqdist]
    return -sum(p * math.log(p,2) for p in probs) 