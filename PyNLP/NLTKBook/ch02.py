""" --- Chapter 2. Accessing Text Corpora and Lexical Resources --- 
    What are some useful text corpora and lexical resources, and how can we access them with Python?
    Which Python constructs are most helpful for this work?
    How do we avoid repeating ourselves when writing Python code?"""

import nltk
from nltk.corpus import wordnet as wn

# Brown Corpus
from nltk.corpus import brown
news_text = brown.words(categories='news')
fdist = nltk.FreqDist(w.lower() for w in news_text)
modals = ['can', 'could', 'may', 'might', 'must', 'will']
for m in modals:
    print (m + ':', fdist[m])

# Conditional frequency distributions
cfd = nltk.ConditionalFreqDist((genre, word)
                               for genre in brown.categories()
                               for word in brown.words(categories=genre))
genres = ['news', 'religion', 'hobbies', 'science_fiction', 'romance', 'humor']
modals = ['can', 'could', 'may', 'might', 'must', 'will']
#cfd.tabulate(conditions=genres, samples=modals)

# Transform singular to plural noun
def plural(word):
    if word.endswith('y'):
        return word[:-1] + 'ies'
    elif word[-1] in 'sx' or word[-2] in ['sh', 'ch']:
        return word + 'es'
    elif word.endswith('an'):
        return word[:-2] + 'en'
    else:
        return word + 's'

# Stopwords Corpus (high-frequency words) and often filter out of a document before further processing
raw_text = """The most obvious fact about texts that emerges from the preceding examples is that they differ in the vocabulary they use. In this section we will see how to use the computer to count the words in a text in a variety of useful ways. As before, you will jump right in and experiment with the Python interpreter, even though you may not have studied Python systematically yet. Test your understanding by modifying the examples, and trying the exercises at the end of the chapter."""
import re
text = re.findall(r"[\w']+|[.,!?;]", raw_text)
stopwords = nltk.corpus.stopwords.words('english')
content_word = [w for w in text if w.lower() not in stopwords]
print (len(text), len(content_word))

# Names corpus
names = nltk.corpus.names
print (names.fileids())
cfd = nltk.ConditionalFreqDist((fileid, name[-1])
                               for fileid in names.fileids()
                               for name in names.words(fileid))
#cfd.plot()

# WordNet - semantically-oriented dictionary of English
print (wn.Synset('dog.n.01'))
print (wn.Synset('car.n.01').lemma_names())
print (wn.Synset('car.n.01').definition())
print (wn.Synset('car.n.01').examples())

# Unusal words
text_vocab = set(w.lower() for w in raw_text if w.isalpha())
english_vocab = set(w.lower() for w in nltk.corpus.words.words())
unusual_words = text_vocab - english_vocab
    