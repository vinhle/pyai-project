""" --- Chapter 1. Language Processing and Python --- 
- Counting Vocabulary
- Simple Statistics """
# -*- coding: utf-8 -*-

# Install packages from NLTK
from __future__ import division
import re
import nltk
print("Version of nltk package ", nltk.__version__)
#nltk.download()

raw_text = """The most obvious fact about texts that emerges from the preceding examples is that they differ in the vocabulary they use. In this section we will see how to use the computer to count the words in a text in a variety of useful ways. As before, you will jump right in and experiment with the Python interpreter, even though you may not have studied Python systematically yet. Test your understanding by modifying the examples, and trying the exercises at the end of the chapter."""

# Texts as lists of words and punctuation
# text1 = raw_text.split()
text1 = re.findall(r"[\w']+|[.,!?;]", raw_text)
text1 = [word.lower() for word in text1]

# Count the words = words + punctuation symbols (tokens - technical name for a sequence of characters)
words_size = len(text1)
print("The number of words in text: ", words_size)

# Sorted list of vocabulary
vocab = sorted(set(text1))
vocab_size = len(vocab)
print("The number of vocabulary in text: ", vocab_size)

# The lexical richness/ lexical diversity
print("The lexical richness of text: ", vocab_size / words_size)

#  Counting words appearing
word = "the"
print("The word \"%s\" appears %s times." % (word, text1.count(word)))

# Frequency distributions (the frequency of each vocabulary item in the text)
fdist1 = nltk.FreqDist(text1)
#print(fdist1)
print ("The 10 most frequent words ", fdist1.most_common(10))
print ("The most frequent word ", fdist1.max())
print ("The appearance of \"the\" is ", fdist1['the'])
print ("The frequent of \"the\" is ", fdist1.freq('the'))
#fdist1.plot(10, cumulative=False)

# Fine-grained selection of words
long_vocabs = [w for w in vocab if len(w) < 10 and fdist1[w] > 3]
print ("there are %d short vocabulary that appear more than 3 times \n %r" % (len(long_vocabs), long_vocabs)) 

# Collocations and Bigrams
bigrams = list(nltk.bigrams(['more', 'is', 'said', 'than', 'done']))
print (bigrams)
