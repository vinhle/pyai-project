""" --- Chapter 10. Analyzing the Meaning of Sentences ---
    How can we represent natural language meaning so that a computer can process these representations?
    How can we associate meaning representations with an unlimited set of sentences?
    How can we use programs that connect the meaning representations of sentences to stores of knowledge?
"""
import nltk
from nltk import load_parser
from nltk.sem import chat80

# Transform from English sentence to SQL in restricted domain
def QueryDb():
    #grammar = load_parser('grammars/book_grammars/sql0.fcfg')
    grammar = nltk.grammar.FeatureGrammar.fromstring('''
        % start S
        S[SEM=(?np + WHERE + ?vp)] -> NP[SEM=?np] VP[SEM=?vp]
        VP[SEM=(?v + ?pp)] -> IV[SEM=?v] PP[SEM=?pp]
        VP[SEM=(?v + ?ap)] -> IV[SEM=?v] AP[SEM=?ap]
        NP[SEM=(?det + ?n)] -> Det[SEM=?det] N[SEM=?n]
        PP[SEM=(?p + ?np)] -> P[SEM=?p] NP[SEM=?np]
        AP[SEM=?pp] -> A[SEM=?a] PP[SEM=?pp]
        NP[SEM='Country="greece"'] -> 'Greece'
        NP[SEM='Country="china"'] -> 'China'
        NP[SEM='Country="vietnam"'] -> 'Vietnam'
        Det[SEM='SELECT'] -> 'Which' | 'What'
        N[SEM='City FROM city_table'] -> 'cities'
        IV[SEM=''] -> 'are'
        A[SEM=''] -> 'located'
        P[SEM=''] -> 'in'
    ''')
    parser = nltk.FeatureChartParser(grammar)
    query = 'What cities are located in Vietnam'
    trees = list(parser.parse(query.split()))
    print trees[0].label()              # get label from only node S is enough
    sql_query = trees[0].label()['SEM'] # get only feature SEM
    print type(sql_query)
    sql_query = [s for s in sql_query if s]
    sql_query = ' '.join(sql_query)
    print '>', sql_query
    
    city_db = chat80.sql_query('corpora/city_database/city.db', sql_query)
    for row in city_db: print row[0],
#QueryDb()

# Propositional Logic
def PropositionalLogic():
    print nltk.boolean_ops()
    expr = nltk.sem.Expression.fromstring
    print expr('-(P & Q)')
    print expr('P | (R -> Q)')
    
    SnF = expr('SnF')
    NotFnS = expr('-FnS')
    R = expr('SnF -> -FnS')
    prover = nltk.Prover9()
    #prover.prove(NotFnS, [SnF, R])
    
    val = nltk.Valuation([('P', True), ('Q', True), ('R', False)])
    dom = set()
    g = nltk.Assignment(dom)
    m = nltk.Model(dom, val)
    print(m.evaluate('(P & Q)', g))
#PropositionalLogic()

# First-Order Logic
def FirstOrderLogic():
    read_expr   = nltk.sem.Expression.fromstring
    NotFnS      = read_expr('-north_of(f, s)')
    SnF         = read_expr('north_of(s, f)')
    R           = read_expr('all x. all y. (north_of(x, y) -> -north_of(y, x))')
    prover      = nltk.Prover9()
    prover.prove(NotFnS, [SnF, R])
#FirstOrderLogic()

# The Semantics of English Sentences: from Natural Language to First Order Logic
def NLToFOL():
    parser = load_parser('grammars/book_grammars/simple-sem.fcfg', trace=0)
    sentence = 'Angus gives a bone to every dog'
    tokens = sentence.split()
    for tree in parser.parse(tokens):
        print(tree.label()['SEM'])
NLToFOL()