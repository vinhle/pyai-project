import nltk, pickle
from nltk.corpus import brown

""" N-Gram Tagging """

brown_sents = brown.sents(categories='news')
#print brown_sents[1]

# Create data set: train & test
brown_tagged_sents = brown.tagged_sents(categories='news')
size = (int)(len(brown_tagged_sents) * 0.9)
train_set = brown_tagged_sents[:size]
test_set  = brown_tagged_sents[size:]

# Train a tagger model
default_tagger_model = nltk.DefaultTagger('NN')
unigram_tagger_model = nltk.UnigramTagger(train_set, backoff=default_tagger_model)
bigram_tagger_model  = nltk.BigramTagger(train_set)

combine_tagger_model = nltk.BigramTagger(train_set, backoff=unigram_tagger_model)

# Evaluate model
print "Default tagger model: ", nltk.DefaultTagger('NN').evaluate(test_set)
print "Unigram tagger model: ", unigram_tagger_model.evaluate(test_set)
print "Bigram tagger model:  ", bigram_tagger_model.evaluate(test_set)
print "Combine tagger model: ", combine_tagger_model.evaluate(test_set)

# Test case
raw_text = "I come from Vietnam."
tokens = nltk.tokenize.word_tokenize(raw_text, 'English')
print "Unigram tagger model: ", unigram_tagger_model.tag(tokens)
print "Bigram tagger model: ", bigram_tagger_model.tag(tokens)

# Store POS tagger model
fout = open('pos.pkl', 'wb')
pickle.dump(combine_tagger_model, fout, -1)
fout.close()

# Load POS tagger model
fin = open('pos.pkl', 'rb')
tagger = pickle.load(fin)
fin.close()
text = """The board's action shows what free enterprise is up against in our complex maze of regulatory laws ."""
tokens = text.split()
print tagger.tag(tokens)
