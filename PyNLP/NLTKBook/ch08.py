""" --- Chapter 8. Analyzing Sentence Structure ---
    How can we use a formal grammar to describe the structure of an unlimited set of sentences?
    How do we represent the structure of sentences using syntax trees?
    How do parsers analyze a sentence and automatically build a syntax tree?
"""

import nltk

# Syntactic Parser by Phrase Structure Grammar
def PhraseStructureGrammar():
    groucho_grammar = nltk.CFG.fromstring("""
        S -> NP VP
        PP -> P NP
        NP -> Det N | Det N PP | 'I'
        VP -> V NP | VP PP
        Det -> 'an' | 'my'
        N -> 'elephant' | 'pajamas'
        V -> 'shot'
        P -> 'in'
    """)
    sent   = ['I', 'shot', 'an', 'elephant', 'in', 'my', 'pajamas']
    parser = nltk.ChartParser(groucho_grammar)
    for tree in parser.parse(sent):
        print(tree)
        tree.draw()
    #nltk.app.rdparser() # Recursive Descent Parser Demo Program
    #nltk.app.srparser() # Shift-Reduce Parser Demo Program
#PhraseStructureGrammar()

# Dependencies Parser by Dependency Grammar
def DependencyGrammar():
    groucho_dep_grammar = nltk.DependencyGrammar.fromstring("""
        'shot' -> 'I' | 'elephant' | 'in'
        'elephant' -> 'an' | 'in'
        'in' -> 'pajamas'
        'pajamas' -> 'my'
        """)
    pdp = nltk.ProjectiveDependencyParser(groucho_dep_grammar)
    sent = 'I shot an elephant in my pajamas'.split()
    trees = pdp.parse(sent)
    for tree in trees:
        print(tree)
        tree.draw()
#DependencyGrammar()

# Probabilistic Context Free Grammar
def PCFG():
    grammar = nltk.PCFG.fromstring("""
    S    -> NP VP              [1.0]
    VP   -> TV NP              [0.4]
    VP   -> IV                 [0.3]
    VP   -> DatV NP NP         [0.3]
    TV   -> 'saw'              [1.0]
    IV   -> 'ate'              [1.0]
    DatV -> 'gave'             [1.0]
    NP   -> 'telescopes'       [0.8]
    NP   -> 'Jack'             [0.2]
    """)
    viterbi_parser = nltk.ViterbiParser(grammar)
    for tree in viterbi_parser.parse(['Jack', 'saw', 'telescopes']):
        print(tree)
PCFG()