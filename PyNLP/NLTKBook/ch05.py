""" --- Chapter 5. Categorizing and Tagging Words ---
    What are lexical categories and how are they used in natural language processing?
    What is a good Python data structure for storing words and their categories?
    How can we automatically tag each word of a text with its word class?
"""
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import brown

# Using a Tagger
text = word_tokenize("And now for something completely different")
print nltk.pos_tag(text)
nltk.help.upenn_tagset('RB')

# Tagged Corpora
# Representing Tagged Tokens
sent = "The/AT grand/JJ jury/NN commented/VBD on/IN a/AT number/NN of/IN"
print [nltk.tag.str2tuple(t) for t in sent.split()]
# Reading Tagged Corpora
brown_news_tagged = brown.tagged_words(categories='news', tagset='universal')
tag_fd = nltk.FreqDist(tag for (word, tag) in brown_news_tagged)
tag_fd.most_common()
tag_fd.plot(cumulative=True)

# File type of word that is in before of NOUN
word_tag_pairs = nltk.bigrams(brown_news_tagged)
noun_preceders = [a[1] for (a,b) in word_tag_pairs if b[1] == 'NOUN']
fdist = nltk.FreqDist(noun_preceders)
[tag for (tag, _) in fdist.most_common()]

# How the word "often" is used in text
brown_lrnd_tagged = brown.tagged_words(categories='learned', tagset='universal')
tags = [b[1] for (a,b) in nltk.bigrams(brown_lrnd_tagged) if a[0]== 'often']
fd = nltk.FreqDist(tags)
fd.tabulate()

# Find the pair of word in form: <verb> to <verb>
sent = "He want to become a singer."
for (w1,t1), (w2,t2), (w3,t3) in nltk.trigrams(sent):
    if(t1.startwith('V') and t2.startwith('V') and t3.startwith('V')):
        print w1, w2, w3

# Find all POS of a word from tagged corpus
brown_news_tagged = brown.tagged_words(categories='news', tagset='universal')
data = nltk.ConditionalFreqDist((word.lower(), tag) for (word, tag) in brown_news_tagged)
for word in sorted(data.condition()):
    if len(data[word]) > 3:
        tags = [tag for (tag, _) in data[word].most_common()]
        print(word, ' '.join(tags))