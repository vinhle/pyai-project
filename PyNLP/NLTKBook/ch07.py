""" --- Chapter 7. Extracting Information from Text ---    
    How can we build a system that extracts structured data, such as tables, from unstructured text?
    What are some robust methods for identifying the entities and relationships described in a text?
    Which corpora are appropriate for this work, and how do we use them for training and evaluating our models?
"""

import nltk
from nltk.corpus import conll2000

def ie_preprocess(doct):
    sents = nltk.sent_tokenize(doct)
    sents = [nltk.word_tokenize(sent) for sent in sents]
    sents = [nltk.pos_tag(sent) for sent in sents]
    for sent in sents: print "POS Tagging: ", sent
    return sents

def NP_Chunking(sents): # by regular expression
    grammar1 = "NP: {<DT>?<JJ>*<NN>}"
    grammar2 = r"""NP: {<DT|PP\$>?<JJ.*>*<NN>}   # chunk determiner/possessive, adjectives and noun
                       {<NNP>+}                  # chunk sequences of proper nouns """
    grammar3 = r"""NP:  {<.*>+}          # Chunk everything
                        }<VBD|IN>+{      # Chink sequences of VBD and IN """
    cp = nltk.RegexpParser(grammar2)
    for sent in sents:
        result = cp.parse(sent)
        print(result); result.draw()

def nltk_tree(): # Present tree structure by NLTK
    tree1 = nltk.Tree('NP', ['Alics'])
    tree2 = nltk.Tree('NP', ['the', 'rabbit'])
    tree3 = nltk.Tree('VP', ['chased', tree2])
    tree4 = nltk.Tree('S', [tree1, tree3])
    print tree4
    print tree4[1].label()
    print tree4.leaves()
    tree4.draw()
    tranverse(tree4)

def tranverse(t):
    try: t.label()
    except AttributeError:
        print t + " ",
    else:
        print '(' + t.label() + " ",
        for child in t: tranverse(child)
        print ')' + " ",

def nltk_ne(): # Name Entity
    sent = nltk.corpus.treebank.tagged_sents()[22]
    print(nltk.ne_chunk(sent, binary=True))
    print(nltk.ne_chunk(sent))
    nltk.ne_chunk(sent).draw()
    
sents = """ the little yellow dog barked at the  cat"""
#sents = ie_preprocess(sents)
#NP_Chunking(sents)
#nltk_tree()
nltk_ne()

