""" --- Chapter 9. Building Feature Based Grammars ---
    How can we extend the framework of context free grammars with features so as to gain more fine-grained control over grammatical categories and productions?
    What are the main formal properties of feature structures and how do we use them computationally?
    What kinds of linguistic patterns and grammatical constructions can we now capture with feature based grammars?
"""

import nltk
from nltk import load_parser

# Syntactic Agreement & Feature Based Grammars (feature can be atomic or structure value)
def FeatureGrammar():
    # cp = load_parser('db/feat.fcfg', trace=2) Load from grammar file
    sents = 'Kim likes children'.split()
    grammar = nltk.grammar.FeatureGrammar.fromstring('''
        % start S
        # ###################
        # Grammar Productions
        # ###################
        # S expansion productions
        S -> NP[NUM=?n] VP[NUM=?n]
        # NP expansion productions
        NP[NUM=?n] -> N[NUM=?n] 
        NP[NUM=?n] -> PropN[NUM=?n] 
        NP[NUM=?n] -> Det[NUM=?n] N[NUM=?n]
        NP[NUM=pl] -> N[NUM=pl] 
        # VP expansion productions
        VP[TENSE=?t, NUM=?n] -> IV[TENSE=?t, NUM=?n]
        VP[TENSE=?t, NUM=?n] -> TV[TENSE=?t, NUM=?n] NP
        # ###################
        # Lexical Productions
        # ###################
        Det[NUM=sg] -> 'this' | 'every'
        Det[NUM=pl] -> 'these' | 'all'
        Det -> 'the' | 'some' | 'several'
        PropN[NUM=sg]-> 'Kim' | 'Jody'
        N[NUM=sg] -> 'dog' | 'girl' | 'car' | 'child'
        N[NUM=pl] -> 'dogs' | 'girls' | 'cars' | 'children' 
        IV[TENSE=pres,  NUM=sg] -> 'disappears' | 'walks'
        TV[TENSE=pres, NUM=sg] -> 'sees' | 'likes'
        IV[TENSE=pres,  NUM=pl] -> 'disappear' | 'walk'
        TV[TENSE=pres, NUM=pl] -> 'see' | 'like'
        IV[TENSE=past] -> 'disappeared' | 'walked'
        TV[TENSE=past] -> 'saw' | 'liked'
    ''')
    feature_chart_parser = nltk.FeatureChartParser(grammar)
    for tree in feature_chart_parser.parse(sents):
        tree.draw()
        print(tree)
#FeatureGrammar()

# Processing Feature Structures: Subsumption and Unification
def ProcFeatureStruct():
    fs1 = nltk.FeatStruct(PER=3, NUM='p1', GND='fem')
    fs2 = nltk.FeatStruct(POS='N', AGR=fs1)
    print 'Feature structure 1:\n', fs2['AGR']
    print 'Feature structure 2:\n', fs2
    
    fs3 = nltk.FeatStruct("""[  NAME=Lee,
                                ADDRESS=[NUMBER=74,
                                STREET='rue Pascal'],
                                SPOUSE= [NAME=Kim,
                                        ADDRESS=[NUMBER=74,
                                                STREET='rue Pascal']]]""")
    fs4 = nltk.FeatStruct("[SPOUSE = [ADDRESS = [CITY = Paris]]]")
    print 'Feature structure 3:\n', fs3.unify(fs4)
ProcFeatureStruct()

# Extending a Feature based Grammar
'''
- Subcategorization
- Heads Revisited
- Auxiliary Verbs and Inversion
- Unbounded Dependency Constructions
'''
    