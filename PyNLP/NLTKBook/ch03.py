""" --- Chapter 3. Processing Raw Text --- 
    How can we write programs to access text from local files and from the web, in order to get hold of an unlimited range of language material?
    How can we split documents up into individual words and punctuation symbols, so we can carry out the same kinds of analysis we did with text corpora in earlier chapters?
    How can we write programs to produce formatted output and save it in a file?
"""
from __future__ import division
import nltk, re, pprint, urllib2
from nltk import word_tokenize
from bs4 import BeautifulSoup

""" Accessing Text from the Web and from Disk """
# Electronic Books
url = "http://www.gutenberg.org/files/2554/2554.txt"
response = urllib2.urlopen(url)
raw = response.read().decode('utf8')
# Tokenization: break up the string into words and punctuation
tokens = word_tokenize(raw)
print type(tokens), len(tokens)
print tokens[:10]
# Take the further step of creating an NLTK text from this list
nltk_text = nltk.Text(tokens)
print type(nltk_text)
nltk_text.collocations(10, 2)

# Dealing with HTML
url = "http://news.bbc.co.uk/2/hi/health/2284783.stm"
html = urllib2.urlopen(url).read().decode('utf8')
raw = BeautifulSoup(html).get_text()
tokens = word_tokenize(raw)
print tokens

# Regular Expressions for Detecting Word Patterns with re.search
wordlist = [w for w in nltk.corpus.words.words('en') if w.islower()]
reg = [w for w in wordlist if re.search('^[a-z]+[v]$', w)]
print reg
# Application of regular expressions: Extracting Word Pieces + Finding Word Stems with re.findall
wsj = sorted(set(nltk.corpus.treebank.words()))
fd  = nltk.FreqDist(vs for word in wsj
                        for vs in re.findall(r'[aeiou]{2,}', word))
print fd.most_common(10)